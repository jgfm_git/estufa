<?php

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

$id_estoque = $_GET['id'];

$sql = "select 
        e.id,
        f.id as id_fornecedor,
        s.id as id_semente,
        e.quantidade,
        e.valor,
        e.lote,
        s.descricao,
        f.nome
        from entrada as e
        inner join semente as s on
        s.id = e.id_semente
        inner join fornecedor as f on
        f.id = e.id_fornecedor
        where e.id = $id_estoque
        ";
$res = mysqli_query($conn,$sql);

$data = array();
while($row = mysqli_fetch_array($res)){
	array_push($data,array('id_semente' => $row['id_semente']));
	array_push($data,array('quantidade' => $row['quantidade']));
	array_push($data,array('valor' => $row['valor']));
	array_push($data,array('id_fornecedor' => $row['id_fornecedor']));
	array_push($data,array('lote' => $row['lote']));
}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
?>