<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário

$id_responsavel = $_POST['id_responsavel'];
$tipo_responsavel = $_POST['tipo_responsavel'];
$id_entrada = $_POST['id_entrada'];
$quantidadeSemeacao = $_POST['quantidadeSemeacao'];

if ($tipo_responsavel == "Cliente") {
	$saida = "cliente";
	$sql = "SELECT
				s.descricao,
				c.razao_social,
				e.quantidade,
				e.lote
			FROM
				entrada as e
				INNER JOIN cliente as c on
				e.id_responsavel = c.id
				INNER JOIN semente as s on
				e.id_semente = s.id
			WHERE 
				e.id = $id_entrada and
				c.id = $id_responsavel
			";
}else if ($tipo_responsavel == "Fornecedor") {
	$saida = "estufa";
	$sql = "SELECT
				*
			FROM
				entrada as e
			WHERE 
				id_entrada = $id_entrada and
			";
}

$res = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($res)){
	$variedade = $row['descricao'];
	$responsavel = $row['razao_social'];
	$quantidadeEstoque = $row['quantidade'];
	$lote = $row['lote'];
}

if ($quantidadeEstoque < $quantidadeSemeacao){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Quantidade a ser semeada invalida</div>";
	header("Location: ../index.php#estoque_$saida");
}else{
	//CADASTRAR SEMEAÇÃO
	$sql = "INSERT INTO semeacao(variedade,quantidade,lote,responsavel,tipo_responsavel)
			VALUES('$variedade',$quantidadeSemeacao,'$lote','$responsavel','$tipo_responsavel')";
	$res = mysqli_query($conn,$sql);

	//UPDATE NO ESTOQUE
	$sql = "UPDATE";
	if($res){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Semeação cadastrada com sucesso</div>";
		header("Location: ../index.php#estoque_$saida");
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar semeação</div>";
		header("Location: ../index.php#estoque_$saida");
	}
}

mysqli_close($conn);

?>