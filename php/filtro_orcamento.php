<?php
session_start();
require_once("../conn/conexao.php");


if(!empty($_SESSION['ZWxldHJpY2Ft'])){
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
}else{
	header('Location: login.php');
}

$aprovado = $_GET['aprovado'] != null ? $_GET['aprovado'] : 0;

$sql = "SELECT 
			o.id,
			o.cliente,
			o.status,
			o.data_cad
			FROM 
			orcamento as o 
			inner join orcamento_prod as op ON
			o.id = op.orcamento_id
		where 
			o.status = $aprovado
		group by o.id
		";
$res = mysqli_query($conn,$sql);

?>
 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th width="5%">#</th>
        <th>Cliente</th>
        <th width="10%">Status</th>
        <th width="10%">Data de Inclusão</th>
        <th width="10%">Orçamento</th>
        <th width="11%">Aprovação</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th width="5%">#</th>
        <th>Cliente</th>
        <th width="10%">Status</th>
        <th width="10%">Data de Inclusão</th>
        <th width="10%">Orçamento</th>
        <th width="11%">Aprovação</th>
    </tr>
    </tfoot>
    <tbody>
        <?php
            
        while($row = mysqli_fetch_array($res)) { 
            $status = $row['status'];

            if($status == 0){
                $status = "Aguardando retorno";
            }
            if($status == 1){
                $status = "Aprovado";
            }
            if($status == 2){
                $status = "Reprovado";
            }

            ?>
            <tr>
                <td><?php echo $row['id'];?></td>
                <td><?php echo $row['cliente'];?></td>
                <td><?php echo $status;?></td>
                <td><?php echo date('d/m/Y',strtotime($row['data_cad']));?></td>
                <td>
                <center>
                <a class="btn btn-primary btn-circle" target="_BLANK" href="views/print_orcamento.php?id=<?php echo $row['id'];?>" >
                    <i class="fas fa-print" ></i>
                </a>
                </center>
                </td>
                <?php if($row['status'] == 1){ ?>
                <td>
                    Aprovado
                </td>
                <?php }?>
                <?php if($row['status'] == 2){ ?>
                <td>
                    Reprovado
                </td>
                <?php }?>
                <?php if($row['status'] == 0){ ?>
                <td>
                <center>
                <a class="btn btn-danger btn-circle" href="php/reprova_orcamento.php?id=<?php echo $row['id'];?>" >
                    <i class="fas fa-times" ></i>
                </a>
                <a class="btn btn-success btn-circle" href="php/aprova_orcamento.php?id=<?php echo $row['id'];?>" >
                    <i class="fas fa-check" ></i>
                </a>
                </center>
                </td>
                <?php }?>
            </tr>
        <?php }?>	
    </tbody>
</table>
