<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

function getCliente(){
	global $conn;

	$sql = "select id from cliente order by id desc limit 1";
	$res = mysqli_query($conn,$sql);

	while($row = mysqli_fetch_array($res)){
		$id = $row['id'];
	}

	return $id;
}

//Receber os dados do formulário
$razao			= $_POST['razao'];
$data_nasc		= $_POST['data_nasc'];
$cnpj			= $_POST['cnpj'];
$pai			= $_POST['pai'];
$mae			= $_POST['mae'];
$email			= $_POST['email'];
$rg 			= $_POST['rg'] == NULL ? 'NULL' : $_POST['rg'];
$responsavel	= $_POST['responsavel'];
$cep    		= $_POST['cep'];
$endereco		= $_POST['endereco'];
$numero 		= $_POST['numero'];
$bairro 		= $_POST['bairro'];
$cidade 		= $_POST['cidade'];
$barraca 		= $_POST['barraca'];
$telefone		= $_POST['telefone'];
$telefone2		= $_POST['telefone2'] == NULL ? 'NULL' : $_POST['telefone2'];
$observacao		= $_POST['observacao'] == NULL ? 'NULL' : $_POST['observacao'];

if(empty($_POST['cnpj']) || empty($_POST['razao']) || empty($_POST['endereco']) || empty($_POST['bairro'])  || empty($_POST['cidade']) || empty($_POST['numero']) || empty($_POST['cep']) || empty($_POST['telefone']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#cliente"); 
}else{
	
	//Salvar no BD
	$result_data = "INSERT INTO cliente(razao_social,pai,mae,data_nasc,cnpj,rg,responsavel,email,cep,endereco,numero,bairro,cidade,barraca,telefone,telefone2,observacao) 
	value('$razao','$pai','$mae','$data_nasc','$cnpj','$rg','$responsavel','$email','$cep','$endereco','$numero','$bairro','$cidade','$barraca','$telefone','$telefone2',
	'$observacao')";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){
		
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Cliente cadastrado com sucesso</div>";
		header("Location: ../index.php#cliente");		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar cliente</div>";
		header("Location: ../index.php#cliente");
	}
	
}


mysqli_close($conn);


?>