<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

$id = $_GET['id'];

$sql = "select * from cliente where id = $id";

$res = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($res)){
    $razao          = $row['razao_social'];
    $cnpj			= $row['cnpj'];
    $pai            = $row['pai'];
    $mae            = $row['mae'];
    $data_nasc      = $row['data_nasc'];
    $rg 			= $row['rg'];
    $email 			= $row['email'];
    $responsavel	= $row['responsavel'];
    $endereco		= $row['endereco'];
    $numero 		= $row['numero'];
    $bairro 		= $row['bairro'];
    $cidade 		= $row['cidade'];
    $barraca        = $row['barraca'];
    $cep    		= $row['cep'];
    $telefone		= $row['telefone'];
    $telefone2		= $row['telefone2'];
    $observacao		= $row['observacao'];
}

?>
<br>
    <!-- AddProd -->
    
                    <form action="php/edita_cliente.php" method="POST" id="form-cli" >
                        <input type="hidden" id="id_cli_edit" name="id_cli_edit" value="<?= $id ?>">
                            
                        <div class="form-row">
                            <div class="col">
                                <label style="color: grey;">Nome</label>
                                <input name="razao_edit" id="razao_edit" type="text" placeholder="Razão Social / Nome Completo" class="form-control" value="<?= $razao?>" required/><br>
                            </div>
                            <div class="col">
                                <label style="color: grey;">Data de nascimento</label>
                                <input type="date" name="data_nasc_edit" id="data_nasc_edit" class="form-control" value="<?= $data_nasc?>">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input name="pai_edit" id="pai_edit" type="text" placeholder="Nome do pai" class="form-control" value="<?= $pai ?>" requied/><br>
                            </div>
                            <div class="col">
                                <input name="mae_edit" id="mae_edit" type="text" placeholder="Nome da mãe" class="form-control" value="<?= $mae ?>" requied/><br>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col">
                                <input name="cnpj_edit" type="text" placeholder="CNPJ / CPF" class="form-control" value="<?= $cnpj ?>" required /><br>
                            </div>
                            <div class="col">
                                <input name="rg_edit" id="rg_edit" type="text" placeholder="RG" class="form-control" value="<?= $rg ?>" /><br>
                            </div>
                            <div class="col">
                                <input type="email" name="email_edit" id="email_edit" placeholder="Email" class="form-control" value="<?= $email?>"><br>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input name="responsavel_edit" id="responsavel_edit" type="text" placeholder="Responsavel" class="form-control" value="<?= $responsavel ?>" required/><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-4">
                                <input id="cep_edit" name="cep_edit" type="text" placeholder="CEP" class="form-control" value="<?= $cep ?>" required/><br>
                            </div>
                            <div class="col">
                                <input value="Buscar Cep" id="buscaCep" type="button" placeholder="Cep" class="btn btn-primary" /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-8">
                                <input id="endereco_edit" name="endereco_edit" type="text" placeholder="Endereço completo" class="form-control" value="<?= $endereco ?>" required/><br>
                            </div>
                            <div class="col-4">
                                <input name="numero_edit" type="text" placeholder="Numero" class="form-control" value="<?= $numero ?>" required/><br>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col">
                                <input id="bairro_edit" name="bairro_edit" type="text" placeholder="Bairro" class="form-control" value="<?= $bairro ?>" required /><br>
                            </div>
                            <div class="col">
                                <input id="cidade_edit" name="cidade_edit" type="text" placeholder="Cidade" class="form-control" value="<?= $cidade ?>" required /><br>
                            </div>
                            <div class="col">
                                <select class="form-control" name="barraca_edit" id="barraca_edit">
                                    <option value="<?= $barraca ?>"><?= $barraca ?></option>
                                    <?php 
                                        $sql = "SELECT * FROM barraca";
                                        $res = mysqli_query($conn,$sql);
                                        while ($row = mysqli_fetch_array($res)) {
                                            if ($row['nome']!=$barraca) {
                                    ?>
                                            <option value="<?= $row['nome'] ?>"><?= $row['nome'] ?></option>
                                    <?php } } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input name="telefone_edit" type="text" placeholder="Telefone" class="form-control" value="<?= $telefone ?>" required/>
                            </div>
                            <div class="col">
                                <input name="telefone2_edit" type="text" placeholder="Telefone 2 (Opcional)" class="form-control" value="<?= $telefone2 ?>" />
                            </div>
                        </div></br>         
                        <input name="observacao_edit" placeholder="Digite uma observação" class="form-control" value="<?= $observacao ?>" ></textarea><br>
                        
                        
                        <button class="btn btn-success" type="submit" style="float: right">Alterar</button>
                        <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
                    </form>

<script>
        $(document).ready(function () {
            $("#buscaCep").click(function(){

            //Nova variável "cep" somente com dígitos.
            var cep = $("#cep").val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco").val(dados.logradouro);
                            $("#endereco").css("background","#eee");
                            $("#bairro").val(dados.bairro);
                            $("#bairro").css("background","#eee");
                            $("#cidade").val(dados.localidade);
                            $("#cidade").css("background","#eee");
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            });
        });



       
        </script>