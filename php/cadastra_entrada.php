<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário

if ($_POST['collapseEntrada'] == 0) {
	$id_cliente = $_POST['cliente'];
	$tipo_responsavel = "Cliente";
}elseif($_POST['collapseEntrada'] == 1){
	$id_fornecedor = $_POST['fornecedor'];
	$tipo_responsavel = "Fornecedor";
}

$produto_estoque = array();
$qtd_prod_estoque = array();
$valor_estoque	  = array();
$lote = array();

array_push($valor_estoque,$_POST['valor_estoque_0']);
array_push($lote,$_POST['lote_estoque_0']);
$qtd_produto		= $_POST['qtd_produto_estoque'];
array_push($qtd_prod_estoque,$_POST['qtd_prod_estoque_0']);

if (!is_numeric($_POST['produto_estoque_0'])) {
	$produto_estoque_aux = $_POST['produto_estoque_0'];
	//Primeiro insere a semente no bd de Sementes
	$sql = "INSERT INTO semente(descricao,quantidade) 
			VALUES('$produto_estoque_aux',$qtd_prod_estoque[0])";
	$res = mysqli_query($conn,$sql);

	//seleciona o id que foi anteriormente inserido
	$sql = "SELECT * FROM semente where descricao='$produto_estoque_aux' order by id desc limit 1";
	$res= mysqli_query($conn, $sql);
	while ($row = mysqli_fetch_array($res)) {
		$id_semente = $row['id'];
	}
	array_push($produto_estoque,$id_semente);
}else{
	array_push($produto_estoque,$_POST['produto_estoque_0']);
}


if ($qtd_produto>1) {
	for ($i=1; $i < $qtd_produto; $i++) { 
		array_push($valor_estoque,$_POST['valor_estoque_'.$i]);
		array_push($lote,$_POST['lote_estoque_'.$i]);
		array_push($qtd_prod_estoque,$_POST['qtd_prod_estoque_'.$i]);

		if (!is_numeric($_POST['produto_estoque_'.$i])) {
			$produto_estoque_aux = $_POST['produto_estoque_'.$i];
			//Primeiro insere a semente no bd de Sementes
			$sql = "INSERT INTO semente(descricao,quantidade) 
					VALUES('$produto_estoque_aux',$qtd_prod_estoque[$i])";
			$res = mysqli_query($conn,$sql);

			//seleciona o id que foi anteriormente inserido
			$sql = "SELECT * FROM semente where descricao='$produto_estoque_aux' order by id desc limit 1";
			$res= mysqli_query($conn, $sql);
			while ($row = mysqli_fetch_array($res)) {
				$id_semente = $row['id'];
			}
			array_push($produto_estoque,$id_semente);
		}else{
			array_push($produto_estoque,$_POST['produto_estoque_'.$i]);
		}
	}
}

//Validação dos campos
if( empty($_POST['cliente']) && empty($_POST['fornecedor']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#entrada"); 
}else{
	if ($_POST['collapseEntrada']==0) {
		//Verificar nota de entrada com valor total
		$valor = 0;
		for ($i=0; $i < $qtd_produto; $i++) { 
			$valor += $valor_estoque[$i];
		}

		$sql = "INSERT INTO nota_entrada(valor_total) values ($valor)";
		$res = mysqli_query($conn, $sql);

		$sql = "SELECT * FROM nota_entrada order by id desc limit 1";
		$res = mysqli_query($conn, $sql);
		while($row = mysqli_fetch_array($res)) {
			$id_nota = $row['id'];
		}

		//Salvar no BD
		for ($i=0; $i < $qtd_produto; $i++) { 
			$sql = "INSERT INTO entrada(id_semente,id_responsavel,tipo_responsavel,id_nota_entrada,quantidade,valor,lote)
					value($produto_estoque[$i],$id_cliente,'$tipo_responsavel',$id_nota,$qtd_prod_estoque[$i],$valor_estoque[$i],'$lote[$i]')";
			$res = mysqli_query($conn, $sql);

			//Selecionar id da entrada recentemente inserida
			$sqlSelect = "SELECT * FROM entrada WHERE id_semente = $produto_estoque[$i] and quantidade=$qtd_prod_estoque[$i] and lote='$lote[$i]' order by id desc limit 1";
			$resSelect = mysqli_query($conn, $sqlSelect);
			while($row = mysqli_fetch_array($resSelect)) {
				$id_entrada = $row['id'];
			}
			
			//Atualizar quantidade na tabela semente
			$sqlSemente = "UPDATE semente set quantidade=quantidade+$qtd_prod_estoque[$i] where id=$produto_estoque[$i]";
			$resSemente = mysqli_query($conn,$sqlSemente);

			//Inserir no estoque de clientes
			$sqlEstoque = "INSERT INTO estoque_cliente(id_semente,id_cliente,id_entrada,quantidade,lote)
							VALUES($produto_estoque[$i],$id_cliente,$id_entrada,$qtd_prod_estoque[$i],'$lote[$i]')";
			$resEstoque = mysqli_query($conn, $sqlEstoque);
		}
		
	}else if($_POST['collapseEntrada']==1){
		//Verificar nota de entrada com valor total
		$valor = 0;
		for ($i=0; $i < $qtd_produto; $i++) { 
			$valor += $valor_estoque[$i];
		}

		$sql = "INSERT INTO nota_entrada(valor_total) values ($valor)";
		$res = mysqli_query($conn, $sql);

		 $sql = "SELECT * FROM nota_entrada order by id desc limit 1";
		$res = mysqli_query($conn, $sql);
		while($row = mysqli_fetch_array($res)) {
			$id_nota = $row['id'];
		}

		//Salvar no BD
		for ($i=0; $i < $qtd_produto; $i++) { 
			$sql = "INSERT INTO entrada(id_semente,id_responsavel,tipo_responsavel,id_nota_entrada,quantidade,valor,lote)
				value($produto_estoque[$i],$id_fornecedor,'$tipo_responsavel',$id_nota,$qtd_prod_estoque[$i],$valor_estoque[$i],'$lote[$i]')";
			$res = mysqli_query($conn, $sql);

			//Selecionar id da entrada recentemente inserida
			$sqlSelect = "SELECT * FROM entrada WHERE id_semente = $produto_estoque[$i] and quantidade=$qtd_prod_estoque[$i] and lote='$lote[$i]' order by id desc limit 1";
			$resSelect = mysqli_query($conn, $sqlSelect);
			while($row = mysqli_fetch_array($resSelect)) {
				$id_entrada = $row['id'];
			}

			//Atualizar quantidade na tabela semente
			$sqlSemente = "UPDATE semente set quantidade=quantidade+$qtd_prod_estoque[$i] where id=$produto_estoque[$i]";
			$resSemente = mysqli_query($conn,$sqlSemente);

			//Inserir no estoque de estufa
			$sqlEstoque = "INSERT INTO estoque_estufa(id_semente,id_fornecedor,id_entrada,quantidade,lote)
							VALUES($produto_estoque[$i],$id_fornecedor,$id_entrada,$qtd_prod_estoque[$i],'$lote[$i]')";
			$resEstoque = mysqli_query($conn, $sqlEstoque);

		}
	}

	if($res){
		
		
		
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Entrada cadastrada com sucesso</div>";
		header("Location: ../index.php#entrada");		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar entrada</div>";
		header("Location: ../index.php#entrada");
	}
	
}


mysqli_close($conn);

?>