<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");



//Receber os dados do formulário
$id_fornecedor       = $_POST['fornecedor_pagar'];
$valor              = $_POST['valor_pagar'];
$vezes              = $_POST['vezes_pagar'];
$vencimento         = $_POST['vencimento_pagar'];
$descricao          = $_POST['descricao'];



//Validação dos campos
if(empty($_POST['fornecedor_pagar']) || empty($_POST['valor_pagar']) || empty($_POST['vencimento_pagar']) || empty($_POST['vezes_pagar']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#contas_pagar"); 
}else{
    
    for($x = 0;$x < $vezes ; $x++){
        $result_data = "insert into contas_pagar (valor,id_fornecedor,vencimento,descricao,status)
        values ($valor,$id_fornecedor,'$vencimento','$descricao',0)";
        $res = mysqli_query($conn, $result_data);
		
        $vencimento = date('Y-m-d', strtotime('+30 days', strtotime($vencimento)));
    }

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if( $res){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Conta cadastrada com sucesso</div>";
		header("Location: ../index.php#contas_pagar");		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar conta</div>";
		header("Location: ../index.php#contas_pagar");
	}
	
}


mysqli_close($conn);


?>