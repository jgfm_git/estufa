<?php

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário

$id = $_GET['id'];

$sql = "select * from semente ";

$res = mysqli_query($conn,$sql);


?>
<style>
	.selectize-input2 {
    width: 100%;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.1);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.1);
	height: 38px;
	}
</style>
<div class="col-5">
<select name="produto_estoque_<?php echo $id;?>" id="produto_estoque_<?= $id; ?>" class="selectize-input2">
	<?php
	while($row = mysqli_fetch_array($res)){
	$id_prod =  $row['id']; 
	$descricao = $row['descricao'];
	?>
    <option value="<?php echo $id_prod;?>"><?php echo $descricao;?></option>
	<?php
	}
    ?>
</select><br> 
</div>
<input type="hidden" value="<?= $id ?>" id="id_select">
<div class="col-2">
    <input name="qtd_prod_estoque_<?php echo $id;?>" type="number" step="0.01" class="form-control" placeholder="Qtd" />
</div>
<div class="col-2">
    <input type="number" step="0.01" name="valor_estoque_<?php echo $id;?>" id="valor_estoque_<?php echo $id;?>" class="form-control" placeholder="Preço" required>
</div>
<div class="col-2">
    <input type="text" name="lote_estoque_<?php echo $id;?>" id="lote_estoque_<?php echo $id;?>" class="form-control" placeholder="Lote" required>
</div>
<div class="col-1">
    <button class="btn btn-danger" type="button" onclick="remove_prod(<?php echo $id;?>)" ><i class="fas fa-minus"></i></button>
</div>