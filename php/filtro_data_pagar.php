<?php
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['ZWxldHJpY2Ft'])){
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
}else{
	header('Location: login.php');
}

$data1 = $_GET['ini'];
$data2 = $_GET['fim'];

$sql  = "SELECT 
            c.id,
            f.nome,
            c.valor,
            c.vencimento,
			c.descricao,
			c.status
        FROM 
            `contas_pagar` as c
            inner join fornecedor as f ON
            c.id_fornecedor = f.id
        where 
            c.vencimento between '$data1' and '$data2'
		";
$res = mysqli_query($conn,$sql);

?>
 <table class="table table-bordered" id="dataTablePagar" width="100%" cellspacing="0">
  <thead>
    <tr>
      <th>Vencimento</th>
      <th>Valor</th>
      <th>Fornecedor</th>
      <th>Descricao</th>
      <th width="10%">Pagar/Anular</th>
    </tr>
  </thead>
  <tbody>
		<?php
			$total = 0;
		while($row = mysqli_fetch_array($res)) { 
            $responsavel = $row['nome'];
            $total += $row['valor'];
			?>
			<tr>
				<td><?php echo date('d/m/Y',strtotime($row['vencimento']));?></td>
                <td><?php echo "R$ ".number_format($row['valor'], 2, ',','.');?></td>
                <td><?php echo $responsavel;?></td>
                <td><?php echo $row['descricao'];?></td>
				<?php if($row['status'] == 0){ ?>
				<td>
					<center>
						<button class="btn btn-primary btn-circle" onclick="pagar(<?php echo $row['id'];?>)" ><i class="fas fa-download" ></i></button>
						<button class="btn btn-danger btn-circle" onclick="cancel(<?php echo $row['id'];?>)" ><i class="fas fa-window-close" ></i></button>
					</center>
				</td>
				<?php }else if ($row['status']==1) {
				?>
				<td>
					<center>
						Paga
					</center>
				</td>
				<?php 
				}else if ($row['status']==2) {
				 ?>
				<td>
					<center>
						Anulada
					</center>
				</td>
				<?php }?>
			</tr>
		<?php }?>	
  </tbody>
  <tfoot>
    <tr>
      <th>Vencimento</th>
      <th><?php echo "R$ ".number_format($total, 2, ',', '.');?></th>
      <th>Fornecedor</th>
      <th>Descricao</th>
      <th width="10%">Pagar/Anular</th>
    </tr>
  </tfoot>
</table>
