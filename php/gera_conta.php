<?php 
session_start();

include_once("../conn/conexao.php");

//dados do formulário
$id_estoque = $_POST['id_estoque'];
$vencimento	= $_POST['vencimento'];
$pagamento = $_POST['pagamento_id'];

//encontrar forma de pagamento
$sql = "select * from pagamento where id=$pagamento";
$res = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res)) {
    $tipo = $row['tipo'];
    $qtd_vezes = $row['qtd_vezes'];
}


echo $sql = "select 
        sum(e.valor)as valor,
        e.lote,
        e.status,
        e.id_responsavel,
        s.descricao,
        f.nome
        from entrada as e
        inner join semente as s on
        s.id = e.id_semente
        inner join fornecedor as f on
        f.id = e.id_responsavel
        where e.id_nota_entrada = $id_estoque
        group by e.id_nota_entrada
        ";
$res = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res)) {
    $valor = $row['valor'];
    $id_fornecedor = $row['id_responsavel'];
    $descricao = "Produto: ".$row['descricao'].", lote: ".$row['lote'].", fornecedor: ".$row['nome'];
}


//verifica se o pagamento é no boleto
if ($tipo == "BOLETO") {

    //Gera conta de acordo com a quantidade de vezes
    for ($i=0; $i < $qtd_vezes; $i++) { 
        echo $sql = "INSERT INTO contas_pagar(valor,id_estoque,id_fornecedor,vencimento, descricao)
            VALUES($valor/$qtd_vezes,$id_estoque,$id_fornecedor,'$vencimento','$descricao')";
        $res = mysqli_query($conn,$sql);
        $vencimento = date('Y-m-d', strtotime('+30 days', strtotime($vencimento)));
    }

    //Atualiza o status da entrada de Estoque
    $sql = "UPDATE estoque set status=1 where id=$id_estoque";
    $resEstoque = mysqli_query($conn,$sql);
    if ($resEstoque==false) {
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao atualizar estoque</div>";
        //header("Location: ../index.php#contas_pagar");
    }
}else{
    
    //Gera conta
    $sql = "INSERT INTO contas_pagar(valor,id_estoque,id_fornecedor,vencimento, descricao)
            VALUES($valor,$id_estoque,$id_fornecedor,'$vencimento','$descricao')";
    $res = mysqli_query($conn,$sql);

    //Atualiza o status da entrada de Estoque
    $sql = "UPDATE estoque set status=1 where id=$id_estoque";
    $resEstoque = mysqli_query($conn,$sql);
    if ($resEstoque==false) {
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao atualizar estoque</div>";
        //header("Location: ../index.php#contas_pagar");
    }
}

if ($res) {
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-success'>Conta a pagar cadastrada</div>";
	//header("Location: ../index.php#contas_pagar"); 
}else{
    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar conta</div>";
    //header("Location: ../index.php#contas_pagar");
}

?>