<?php

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário

$id_funcionario = $_GET['id_funcionario'];

$sql = "select * from funcionario where id = $id_funcionario";

$res = mysqli_query($conn,$sql);
$data = array();
while($row = mysqli_fetch_array($res)){
	array_push($data,array('nome' => $row['nome']));
	array_push($data,array('endereco' => $row['endereco']));
	array_push($data,array('numero' => $row['numero']));
	array_push($data,array('bairro' => $row['bairro']));
	array_push($data,array('cidade' => $row['cidade']));
	array_push($data,array('cep' => $row['cep']));
	array_push($data,array('telefone' => $row['telefone']));
	array_push($data,array('cpf' => $row['cpf']));
	array_push($data,array('cnh' => $row['cnh']));
	array_push($data,array('validade' => $row['validade']));
}
mysqli_close($conn);
$json = json_encode($data);
echo $json;
?>