<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

if(!empty($_SESSION['ZWxldHJpY2Ft'])){
    $usuario_id = $_SESSION['ZWxldHJpY2Ft'];
}else{
    header('Location: login.php');
}

$id  = $_POST['id_anotacao_edit'];
$assunto  = $_POST['assunto_edit'];
$mensagem = $_POST['mensagem_edit'];

if(empty($_POST['assunto_edit']) || empty($_POST['mensagem_edit'])){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#dashboard"); 
}else{
    $sql = "update anotacao set assunto = '$assunto',mensagem = '$mensagem' where  id = $id";
    $res = mysqli_query($conn,$sql);

    if($res){
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Anotação alterada com sucesso</div>";
		header("Location: ../index.php#dashboard");	
    }else{
        $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao alterar anotação</div>";
		header("Location: ../index.php#dashboard");	
    }
}