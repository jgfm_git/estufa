<?php 
session_start();

include_once("../conn/conexao.php");

//dados do formulário
$id_estoque = $_POST['id_estoque_baixa'];
if ($_POST['vencimento']) {
    $vencimento = "'".$_POST['vencimento']."'";
}else{
    $vencimento = 'now()';
}
$sql = "select 
        e.valor,
        e.lote,
        e.status,
        e.id_fornecedor,
        s.descricao,
        f.nome
        from estoque as e
        inner join semente as s on
        s.id = e.id_semente
        inner join fornecedor as f on
        f.id = e.id_fornecedor
        where e.id = $id_estoque
        ";
$res = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res)) {
    $valor = $row['valor'];
    $id_fornecedor = $row['id_fornecedor'];
    $descricao = "Produto: ".$row['descricao'].", lote: ".$row['lote'].", fornecedor: ".$row['nome'];
}

//Gera conta
$sql = "INSERT INTO contas_pagar(valor,id_estoque,id_fornecedor,vencimento, descricao)
VALUES($valor,$id_estoque,$id_fornecedor,$vencimento,'$descricao')";
$res = mysqli_query($conn,$sql);

//Atualiza o status da entrada de Estoque
$sql = "UPDATE estoque set status=1 where id=$id_estoque";
$resEstoque = mysqli_query($conn,$sql);
if ($resEstoque==false) {
    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao atualizar estoque</div>";
    header("Location: ../index.php#estoque");
}

if ($res) {
    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Baixa realizada</div>";
    header("Location: ../index.php#estoque");
}else{
    $_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-danger'>Erro ao dar baixa</div>";
    header("Location: ../index.php#estoque");
}

?>