<?php
session_start();
include_once("../conn/conexao.php");

$id_orcamento = $_POST['id_orcamento'];
$dataVencimento = $_POST['dataVencimento']; 
$sql = "update orcamento set status = 1 where id = $id_orcamento";
$res = mysqli_query($conn,$sql);

if($res){
    $_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-success'>Aprovado com sucesso</div>";
	header("Location: ../index.php#orcamento"); 
}else{
    $_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Erro ao tentar aprovar</div>";
	header("Location: ../index.php#orcamento"); 
}

$sql = "select
			sum(op.valor*op.qtd) as valor_prod,
			p.qtd_vezes as parcelas,
			p.tipo as tipo,
			o.cliente as responsavel
		from
			orcamento as o
			left join orcamento_prod as op on
			o.id = op.orcamento_id
			inner join pagamento as p on
			o.pagamento_id = p.id  
		where 
			o.id = $id_orcamento
		";
$res = mysqli_query($conn,$sql);
while($row = mysqli_fetch_array($res)) {
	$valor_total = $row['valor_prod'];
	$tipo = $row['tipo'];
	$parcelas = $row['parcelas'];
	$responsavel = $row['responsavel'];
}

$valor_parcela = number_format($valor_total / $parcelas, 2, '.', '');

if ($res) {
	for ($i=0; $i < $parcelas; $i++) { 
		$parcela = ($i+1)."/".$parcelas;
		$sql = "INSERT INTO contas_receber(id_orcamento,responsavel,parcela,valor_parcela,vencimento,tipo)
		VALUES($id_orcamento,'$responsavel','$parcela',$valor_parcela,'$dataVencimento','$tipo')";
		mysqli_query($conn,$sql);
		$dataVencimento = date('Y-m-d', strtotime('+30 days', strtotime($dataVencimento)));
	} 
}else{
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-success'>Erro ao gerar contas a receber</div>";
	header("Location: ../index.php#orcamento"); 
}
?>