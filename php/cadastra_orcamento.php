<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

function getIdOrcamento(){
	global $conn;

	$sql = "select * from orcamento order by id desc limit 1";

	$res = mysqli_query($conn,$sql);

	$id = 0;
	while($row = mysqli_fetch_array($res)){
		$id = $row['id'];
	}

	return $id;
}

function getValorProduto($id){
	global $conn;

	$sql = "select preco from semente where id = $id limit 1";
	$res = mysqli_query($conn,$sql);

	while($row = mysqli_fetch_array($res)){
		$valor = $row['preco'];
	}


	return $valor;

}



//Receber os dados do formulário

if ($_POST['collapseGroup'] == 0) {
	$nome = $_POST['cliente_cad_orc'];
	$cnpj_cliente = $_POST['cliente_cad_cnpj'];
	$telefone_cliente = $_POST['cliente_cad_telefone'];
	$data_nasc_cliente = $_POST['cliente_cad_data_nasc'];
	$endereco_cliente = $_POST['cliente_cad_endereco'];
	$cidade_cliente = $_POST['cliente_cad_cidade'];
	$obs_cliente = $_POST['cliente_cad_obs'];

	$sql = "INSERT INTO cliente(razao_social,data_nasc,cnpj,endereco,cidade,telefone,observacao) 
			VALUES('$nome','$data_nasc_cliente','$cnpj_cliente','$endereco_cliente','$cidade_cliente','$telefone_cliente','$obs_cliente')";
	$res = mysqli_query($conn,$sql);
}

$produto_orc = array();
$qtd_prod_orc = array();
$valor_orc	  = array();

if($_POST['collapseGroup'] == 1){
	$nome			= $_POST['cliente_orc'];
}
array_push($valor_orc,$_POST['valor_orc_0']);
$qtd_produto		= $_POST['qtd_produto'];
array_push($produto_orc,$_POST['produto_orc_0']);
array_push($qtd_prod_orc,$_POST['qtd_prod_0']);
$pagamento			= $_POST['pagamento_orc'];
$observacao			= $_POST['observacao'];
$data_entrega		= $_POST['data_entrega'];


//Validação dos campos
if( empty($_POST['cliente_orc']) && empty($_POST['cliente_cad_orc']) ){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#orcamento"); 
}else{
	//Salvar no BD
 	$result_data = "INSERT INTO orcamento(cliente,pagamento_id,observacao,data_entrega)
	 				 value('$nome',$pagamento,'$observacao','$data_entrega')";
	$resultado_data = mysqli_query($conn, $result_data);

	$id_orcamento = getIdOrcamento();


	if($qtd_produto > 1){
		for($i = 1;$i < $qtd_produto; $i++){
			array_push($produto_orc,$_POST['produto_orc_'.$i]);
			array_push($qtd_prod_orc,$_POST['qtd_prod_orc_'.$i]);
		}
	}


	foreach($produto_orc as $indice => $valor){
		$produto_id 	= $produto_orc[$indice];
		$qtd	 		= $qtd_prod_orc[$indice];
		$valor 			= getValorProduto($produto_id);


		if(!empty($produto_id) || !empty($qtd) || !empty($valor)){
			$sql = "insert into orcamento_prod(produto_id,orcamento_id,qtd,valor) values($produto_id,$id_orcamento,$qtd,$valor)";
			mysqli_query($conn,$sql);
		}
	}

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if(mysqli_insert_id($conn)){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Orçamento cadastrado com sucesso</div>";
		echo "<script>window.open('../views/print_orcamento.php?id=$id_orcamento')</script>";
		echo "<script>window.location.href = '../index.php#orcamento'</script>";
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao cadastrar orçamento</div>";
		header("Location: ../index.php#orcamento");
	}
	
}


mysqli_close($conn);


?>