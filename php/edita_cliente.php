<?php
session_start();

//Incluir a conexão com o BD
include_once("../conn/conexao.php");

//Receber os dados do formulário
$id 			= $_POST['id_cli_edit'];
$razao 			= $_POST['razao_edit'];
$data_nasc		= $_POST['data_nasc_edit'];
$pai            = $_POST['pai_edit'];
$mae            = $_POST['mae_edit'];
$cnpj			= $_POST['cnpj_edit'];
$rg				= $_POST['rg_edit'];
$email			= $_POST['email_edit'];
$responsavel 	= $_POST['responsavel_edit'];
$cep			= $_POST['cep_edit'];
$endereco		= $_POST['endereco_edit'];
$numero			= $_POST['numero_edit'];
$bairro			= $_POST['bairro_edit'];
$cidade			= $_POST['cidade_edit'];
$barraca		= $_POST['barraca_edit'];
$telefone		= $_POST['telefone_edit'];
$telefone2		= $_POST['telefone2_edit'];
$observacao		= $_POST['observacao_edit'];


//Validação dos campos
if(empty($_POST['id_cli_edit']) || empty($_POST['cnpj_edit']) || empty($_POST['razao_edit'])  || empty($_POST['responsavel_edit'])){
	$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-warning'>Preencha os campos corretamente</div>";
	header("Location: ../index.php#cliente"); 
}else{
	//Salvar no BD
  	echo  $result_data = "update cliente set razao_social = '$razao',pai = '$pai',mae = '$mae',data_nasc = '$data_nasc', cnpj = '$cnpj' , rg = '$rg' , 
  		responsavel = '$responsavel', email = '$email', cep = '$cep', endereco = '$endereco',numero = '$numero',bairro = '$bairro',cidade = '$cidade', 
  		barraca = '$barraca',telefone = '$telefone', telefone2 = '$telefone2', observacao = '$observacao' where id = $id ";
	$resultado_data = mysqli_query($conn, $result_data);

	//Verificar se salvou no banco de dados através do "mysqli_insert_id" que verifica se existe o ID do ultimo dado inserido
	if($resultado_data){
		$_SESSION['msg'] = "<div style='margin-left: 10px; margin-right: 10px' class='alert alert-success'>Cliente alterado com sucesso</div>";
		header("Location: ../index.php#cliente");		
	}else{
		$_SESSION['msg'] = "<div style='margin-left: 10px;margin-right: 10px' class='alert alert-danger'>Erro ao alterar cliente</div>";
		header("Location: ../index.php#cliente");
	}
	
}


mysqli_close($conn);


?>