<?php
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['ZWxldHJpY2Ft'])){
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
}else{
	header('Location: login.php');
}

$data1 = $_GET['ini'];
$data2 = $_GET['fim'];

$sql = " select
			cr.id as id,
			cr.id_orcamento as id_orcamento,
			cr.responsavel as responsavel,
			cr.parcela as parcela,
			cr.valor_parcela as valor_parcela,
			cr.vencimento as vencimento,
			cr.tipo as tipo,
			cr.status as status,
			cr.banco as banco
		from
			orcamento as o inner join contas_receber as cr on
            cr.id_orcamento = o.id
        where
            cr.vencimento between '$data1' and '$data2'
		";
$res = mysqli_query($conn,$sql);

?>
 <table class="table table-bordered" id="dataTablePagar" width="100%" cellspacing="0">
    <thead>
      <tr>
        <th>Responsável</th>
        <th>Data de Vencimento</th>
        <th>Parcela</th>
        <th>Valor da Parcela</th>
        <th>Tipo</th>
        <th>Status</th>
        <th>Confirmação</th>
      </tr>
    </thead>
    <tfoot>
		<tr>
        <th>Responsável</th>
        <th>Data de Vencimento</th>
        <th>Parcela</th>
        <th>Valor da Parcela</th>
        <th>Tipo</th>
        <th>Status</th>
        <th>Confirmação</th>
      </tr>
    </tfoot>
    <tbody>
		<?php
							
		while($row = mysqli_fetch_array($res)) { 
			$status = $row['status'];

			if($status == 0){
				$status = "Em aberto";
			}
			if($status == 1){
				$status = "Recebido";
			}
			if($status == 2){
				$status = "Cancelado";
			}

			?>
		<tr>
			<td><?= $row['responsavel'];?></td>
			<td><?= date('d/m/Y',strtotime($row['vencimento']));?></td>
			<td><?= $row['parcela'];?></td>
			<td><?= number_format($row['valor_parcela'],2,',','.');?></td>
			<td><?= $row['tipo'];?></td>
			<td><?= $status;?></td>
			<?php if($row['status'] == 0){ ?>
		            <td>
		            <center>
						<a class="btn btn-danger btn-circle" href="php/reprova_conta.php?id=<?php echo $row['id'];?>" >
							<i class="fas fa-times" ></i>
						</a>
						<a class="btn btn-success btn-circle" onclick="aprovarConta(<?= $row['id'];?>)">
							<i class="fas fa-check" ></i>
						</a>
		            </center>
		            </td>
		            <?php }else{?>
						<td>
							<center>
								<?= $status ?>
							</center>
						</td>
					<?php } ?>
		</tr>
		<?php }?>	
    </tbody>
 </table>
