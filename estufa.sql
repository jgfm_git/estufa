-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 04-Nov-2020 às 22:13
-- Versão do servidor: 10.4.8-MariaDB
-- versão do PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `estufa`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `anotacao`
--

CREATE TABLE `anotacao` (
  `id` int(11) NOT NULL,
  `assunto` varchar(255) DEFAULT NULL,
  `mensagem` longtext DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `data_cad` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `banco`
--

CREATE TABLE `banco` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `valor` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `banco`
--

INSERT INTO `banco` (`id`, `nome`, `valor`) VALUES
(1, 'Caixa - Estufa', '96400');

-- --------------------------------------------------------

--
-- Estrutura da tabela `barraca`
--

CREATE TABLE `barraca` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `razao_social` varchar(100) DEFAULT NULL,
  `pai` varchar(150) DEFAULT NULL,
  `mae` varchar(150) DEFAULT NULL,
  `data_nasc` date DEFAULT NULL,
  `cnpj` varchar(45) DEFAULT NULL,
  `rg` varchar(100) DEFAULT NULL,
  `responsavel` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cep` varchar(50) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `numero` varchar(20) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `barraca` varchar(100) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `telefone2` varchar(45) DEFAULT NULL,
  `observacao` longtext DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `data_cad` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id`, `razao_social`, `pai`, `mae`, `data_nasc`, `cnpj`, `rg`, `responsavel`, `email`, `cep`, `endereco`, `numero`, `bairro`, `cidade`, `barraca`, `telefone`, `telefone2`, `observacao`, `status`, `data_cad`) VALUES
(2, 'EvolutionSoft', 'pai', 'mae', '2001-08-31', '1800000', '9135', 'responsaw', 'emailll@gmail.com', '14000', 'Endereço', '3150', 'bairro', 'barraca', 'Barraca', 'telefone1', 'telefone2', 'algo', 1, '2020-10-21 21:47:33'),
(3, 'JOAO GABRIEL F', 'ANDERSON', 'KARINA', '2001-09-01', '00000', '9999931', 'responsavel', 'JOAO@GMAIL.COMe', '000000', 'RuaAA', '524', 'Vila AdamantiumM', 'AdamantinaA', 'Barraca2', '1599730807000', '315900020', 'algoO', 1, '2020-10-21 21:47:31'),
(5, 'Luiz A', NULL, NULL, '1992-02-16', '102000', NULL, NULL, NULL, NULL, 'endereco completo', NULL, NULL, 'cidade', NULL, '10000', NULL, 'observacao cliente', 1, '2020-10-21 21:47:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente_inativo`
--

CREATE TABLE `cliente_inativo` (
  `id` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `data_cad` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contas_pagar`
--

CREATE TABLE `contas_pagar` (
  `id` int(11) NOT NULL,
  `valor` float DEFAULT NULL,
  `id_estoque` int(11) DEFAULT NULL,
  `id_fornecedor` int(11) DEFAULT NULL,
  `vencimento` date DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `comprovante` longblob DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `data_cad` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `contas_receber`
--

CREATE TABLE `contas_receber` (
  `id` int(11) NOT NULL,
  `id_orcamento` int(11) NOT NULL,
  `responsavel` varchar(100) NOT NULL,
  `parcela` varchar(20) NOT NULL,
  `valor_parcela` float NOT NULL,
  `vencimento` date NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `obs_cancel` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `banco` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `entrada`
--

CREATE TABLE `entrada` (
  `id` int(11) NOT NULL,
  `id_semente` int(11) DEFAULT NULL,
  `id_responsavel` int(11) DEFAULT NULL,
  `tipo_responsavel` varchar(20) NOT NULL,
  `id_nota_entrada` int(11) DEFAULT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `lote` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `data_cad` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `estoque_cliente`
--

CREATE TABLE `estoque_cliente` (
  `id` int(11) NOT NULL,
  `id_semente` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_entrada` int(11) DEFAULT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `lote` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `estoque_estufa`
--

CREATE TABLE `estoque_estufa` (
  `id` int(11) NOT NULL,
  `id_semente` int(11) DEFAULT NULL,
  `id_fornecedor` int(11) NOT NULL,
  `id_entrada` int(11) DEFAULT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `lote` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `fornecedor`
--

CREATE TABLE `fornecedor` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `cnpj` varchar(100) DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `numero` varchar(50) NOT NULL,
  `bairro` varchar(100) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `cep` varchar(50) NOT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `telefone2` varchar(45) DEFAULT NULL,
  `data_cad` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `fornecedor`
--

INSERT INTO `fornecedor` (`id`, `nome`, `cnpj`, `endereco`, `numero`, `bairro`, `cidade`, `cep`, `telefone`, `telefone2`, `data_cad`) VALUES
(1, 'fornecedor', '1123', 'end', '132', 'bairroo', 'cid', '315415', '135', '315153', '2020-10-05 22:04:05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE `funcionario` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cpf` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `endereco` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bairro` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cep` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cnh` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validade` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `log` longtext NOT NULL,
  `data_cad` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `nota_entrada`
--

CREATE TABLE `nota_entrada` (
  `id` int(11) NOT NULL,
  `valor_total` float DEFAULT NULL,
  `data_cad` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamento`
--

CREATE TABLE `orcamento` (
  `id` int(11) NOT NULL,
  `cliente` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pagamento_id` int(11) NOT NULL,
  `observacao` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `data_entrega` date NOT NULL,
  `data_cad` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `orcamento_prod`
--

CREATE TABLE `orcamento_prod` (
  `id` int(11) NOT NULL,
  `produto_id` int(11) NOT NULL,
  `orcamento_id` int(11) NOT NULL,
  `qtd` int(11) NOT NULL,
  `valor` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pagamento`
--

CREATE TABLE `pagamento` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `qtd_vezes` int(11) NOT NULL,
  `juros` float NOT NULL,
  `taxa_retirada` float NOT NULL,
  `tipo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `semeacao`
--

CREATE TABLE `semeacao` (
  `id_semeacao` int(11) NOT NULL,
  `variedade` varchar(100) DEFAULT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `lote` varchar(100) DEFAULT NULL,
  `responsavel` varchar(100) DEFAULT NULL,
  `tipo_responsavel` varchar(100) NOT NULL,
  `data_cad` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `semente`
--

CREATE TABLE `semente` (
  `id` int(11) NOT NULL,
  `descricao` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preco` float NOT NULL DEFAULT 0,
  `quantidade` int(11) NOT NULL,
  `obs` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_cad` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `avatar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_permission`
--

CREATE TABLE `user_permission` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `permission` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `veiculo`
--

CREATE TABLE `veiculo` (
  `id` int(11) NOT NULL,
  `modelo` varchar(100) DEFAULT NULL,
  `marca` varchar(100) DEFAULT NULL,
  `cor` varchar(100) DEFAULT NULL,
  `ano` int(4) DEFAULT NULL,
  `placa` varchar(100) DEFAULT NULL,
  `renavam` varchar(100) DEFAULT NULL,
  `vencimento` date DEFAULT NULL,
  `km` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `anotacao`
--
ALTER TABLE `anotacao`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `banco`
--
ALTER TABLE `banco`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `barraca`
--
ALTER TABLE `barraca`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cnpj_UNIQUE` (`cnpj`);

--
-- Índices para tabela `cliente_inativo`
--
ALTER TABLE `cliente_inativo`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `contas_pagar`
--
ALTER TABLE `contas_pagar`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `contas_receber`
--
ALTER TABLE `contas_receber`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `entrada`
--
ALTER TABLE `entrada`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `estoque_cliente`
--
ALTER TABLE `estoque_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `estoque_estufa`
--
ALTER TABLE `estoque_estufa`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `fornecedor`
--
ALTER TABLE `fornecedor`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `nota_entrada`
--
ALTER TABLE `nota_entrada`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `orcamento`
--
ALTER TABLE `orcamento`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `orcamento_prod`
--
ALTER TABLE `orcamento_prod`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `pagamento`
--
ALTER TABLE `pagamento`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `semeacao`
--
ALTER TABLE `semeacao`
  ADD PRIMARY KEY (`id_semeacao`);

--
-- Índices para tabela `semente`
--
ALTER TABLE `semente`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `user_permission`
--
ALTER TABLE `user_permission`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `veiculo`
--
ALTER TABLE `veiculo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `anotacao`
--
ALTER TABLE `anotacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `banco`
--
ALTER TABLE `banco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `barraca`
--
ALTER TABLE `barraca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `cliente_inativo`
--
ALTER TABLE `cliente_inativo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `contas_pagar`
--
ALTER TABLE `contas_pagar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `contas_receber`
--
ALTER TABLE `contas_receber`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `entrada`
--
ALTER TABLE `entrada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `estoque_cliente`
--
ALTER TABLE `estoque_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `estoque_estufa`
--
ALTER TABLE `estoque_estufa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `fornecedor`
--
ALTER TABLE `fornecedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `funcionario`
--
ALTER TABLE `funcionario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `nota_entrada`
--
ALTER TABLE `nota_entrada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `orcamento`
--
ALTER TABLE `orcamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `orcamento_prod`
--
ALTER TABLE `orcamento_prod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `pagamento`
--
ALTER TABLE `pagamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `semeacao`
--
ALTER TABLE `semeacao`
  MODIFY `id_semeacao` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `semente`
--
ALTER TABLE `semente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `user_permission`
--
ALTER TABLE `user_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `veiculo`
--
ALTER TABLE `veiculo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
