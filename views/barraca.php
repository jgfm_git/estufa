_barraca<?php 
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['ZWxldHJpY2Ft'])){
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
}else{
	header('Location: login.php');
}


$sql = "select * from barraca";
$res = mysqli_query($conn,$sql);
	
?>  
<style>
	.xx{
				float: right;
				background: #ccc;
				border-radius: 200px;
				width:14px;
				height: 13px;
				color: white;
				text-align: center;
				font-size: 10px;
			}
			.xx:hover{
				background: #777;
				cursor: pointer
			}
			.dataTables_wrapper .dataTables_filter input{
				border-radius: 10px;
				border: 1px solid #ccc;
				outline-style: none;
			}
</style>

   <div class="container-fluid">



          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h4 class="m-0 font-weight-bold text-primary">Barracas
				<button  style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddBarraca" >Adicionar</button>
				
			  </h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nome</th>
                      <th>Telefone</th>
                      <th>Email</th>
                      <th width="10%">Editar</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Nome</th>
                      <th>Telefone</th>
                      <th>Email</th>
                      <th width="10%">Editar</th>
                    </tr>
                  </tfoot>
                  <tbody>
						<?php
						while($row = mysqli_fetch_array($res)) {
						?>
							<tr>
								<td><?php echo $row['nome'];?></td>
								<td><?php echo $row['telefone'];?></td>
                                <td><?php echo $row['email'];?></td>
								<td>
									<center>
										<button class="btn btn-warning btn-circle" onclick="edit_barraca(<?php echo $row['id'];?>)" ><i class="fas fa-edit" ></i></button>
									</center>
								</td>
							</tr>
						<?php }?>	
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
	
		  
		  	<!-- EditProd -->
		<div class="modal fade" id="EditBarraca" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar de Fornecedor</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/edita_barraca.php" method="POST"  >
					
						<input type="hidden" id="id_barraca_edit" name="id_barraca_edit" >

                        <div class="form-row">
                            <div class="col">
                                <input type="text" name="nome_barraca_edit" id="nome_barraca_edit" class="form-control" placeholder="Nome da Barraca"><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <input type="text" name="telefone_barraca_edit" id="telefone_barraca_edit" class="form-control" placeholder="Telefone">
                            </div>
                            <div class="col">
                                <input type="text" name="email_barraca_edit" id="email_barraca_edit" class="form-control" placeholder="Email">
                            </div>
                        </div><br>
                                
						
						<button class="btn btn-success" type="submit" style="float: right">Alterar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
		
		<script>
			$(document).ready(function() {
                $('#dataTable').DataTable( {
                });
            });
                    
		
		function edit_barraca(id){
			$.get( "php/get_barraca.php?id_barraca="+id, function( data ) {
					 var json = JSON.parse(data);
					$( "#id_barraca_edit" ).val( id );
					$( "#nome_barraca_edit" ).val( json[0].nome );
					$( "#telefone_barraca_edit" ).val( json[1].telefone );
					$( "#email_barraca_edit" ).val( json[2].email );
					
					$('#EditBarraca').modal('show');

				});
				
		}
		</script>
		