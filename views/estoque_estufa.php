<?php 
session_start();

require_once("../conn/conexao.php");

function getResponsavel($tipo,$id){
  global $conn;
  if ($tipo=="Fornecedor") {
    $sql = "SELECT nome FROM fornecedor WHERE id=$id";
    $res = mysqli_query($conn,$sql);
  }elseif($tipo=="Cliente"){
    $sql = "SELECT razao_social FROM cliente WHERE id=$id";
    $res = mysqli_query($conn,$sql);
  }
  while($row = mysqli_fetch_array($res)){
    $nome = $row[0];
  }
  return $nome;
}


$sql = "select * from semente";
$resSemente = mysqli_query($conn,$sql);

$sql = "select * from fornecedor";
$resFornecedor = mysqli_query($conn,$sql);

$sql = "select * from pagamento";
$resPagamento = mysqli_query($conn,$sql); 



$sql = "
        SELECT
          'Fornecedor' as tipo_responsavel,
          es.id_fornecedor as id_responsavel,
          es.id_entrada,
          s.descricao,
          es.lote,
          es.quantidade

        FROM estoque_estufa as es
          INNER JOIN fornecedor as f on
          es.id_fornecedor = f.id
          INNER JOIN semente as s on
          es.id_semente = s.id
        ";
$res = mysqli_query($conn,$sql);

?>   
<style>
.onoff input.toggle {
				display: none;
			}

			.onoff input.toggle + label {
				display: inline-block;
				position: relative;
				box-shadow: inset 0 0 0px 1px #d5d5d5;
				height: 20px;
				width: 40px;
				border-radius: 30px;
			}

			.onoff input.toggle + label:before {
				content: "";
				display: block;
				height: 20px;
				width: 40px;
				border-radius: 30px;
				background: rgba(19, 191, 17, 0);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle + label:after {
				content: "";
				position: absolute;
				height: 20px;
				width: 20px;
				top: 0;
				left: 0px;
				border-radius: 30px;
				background: #fff;
				box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle:checked + label:before {
				width: 40px;
				background: #13bf11;
			}

			.onoff input.toggle:checked + label:after {
				left: 20px;
				box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
			}
			.xx{
				float: right;
				background: #ccc;
				border-radius: 200px;
				width:14px;
				height: 13px;
				color: white;
				text-align: center;
				font-size: 10px;
			}
			.xx:hover{
				background: #777;
				cursor: pointer
			}
			.dataTables_wrapper .dataTables_filter input{
				border-radius: 10px;
				border: 1px solid #ccc;
				outline-style: none;
			}
      .show{
				display: block;
			}
			.hide{
				display: none;
			}
</style>
   <div class="container-fluid">

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h4 class="m-0 font-weight-bold text-primary">Estoque Estufa
				<!-- <button  style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#" >Adicionar</button> -->
				
			  </h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th width="5%">Via do Cliente</th>
                      <th>Cliente</th>
                      <th>Variedade</th>
                      <th>Lote</th>
                      <th>Quantidade</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th width="5%">Via do Cliente</th>
                      <th>Cliente</th>
                      <th>Variedade</th>
                      <th>Lote</th>
                      <th>Quantidade</th>
                    </tr>
                  </tfoot>

                  <tbody>
                  <?php
                  while($row = mysqli_fetch_array($res)) {       
                    $tipo_responsavel = $row['tipo_responsavel'];

                  ?>  

							      <tr>
                      <td>
                          <center>
                              <a class="btn btn-primary btn-circle" target="_BLANK" href="views/print_via_responsavel.php?id=<?=$row['id_responsavel'];?>&tipo=<?=$tipo_responsavel?>&view=1">
                              <i class="fas fa-eye"></i></a>
                          </center>
                      </td>
                      <td><?= getResponsavel($row['tipo_responsavel'],$row['id_responsavel']); ?></td>
                      <td><?= $row['descricao']; ?></td>
                      <td><?= $row['lote']; ?></td>
                      <td><?= $row['quantidade']; ?></td>
							      </tr>
						      <?php }?>	
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
		<script>
			$(document).ready(function() {
                $('#dataTable').DataTable( {
                  "aaSorting": [[1,"desc"]]
                });
            }); 
		</script>

