<?php 
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['ZWxldHJpY2Ft'])){
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
}else{
	header('Location: login.php');
}


$sql  = "SELECT 
            c.id,
            f.nome,
            c.valor,
            c.vencimento,
			c.descricao,
			c.status
        FROM 
            `contas_pagar` as c
            inner join fornecedor as f ON
            c.id_fornecedor = f.id
        where 
            month(c.vencimento) = month(now()) and year(c.vencimento) = year(now())
		";
$res = mysqli_query($conn,$sql);



	
?>   
<style>
.onoff input.toggle {
				display: none;
			}

			.onoff input.toggle + label {
				display: inline-block;
				position: relative;
				box-shadow: inset 0 0 0px 1px #d5d5d5;
				height: 20px;
				width: 40px;
				border-radius: 30px;
			}

			.onoff input.toggle + label:before {
				content: "";
				display: block;
				height: 20px;
				width: 40px;
				border-radius: 30px;
				background: rgba(19, 191, 17, 0);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle + label:after {
				content: "";
				position: absolute;
				height: 20px;
				width: 20px;
				top: 0;
				left: 0px;
				border-radius: 30px;
				background: #fff;
				box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle:checked + label:before {
				width: 40px;
				background: #13bf11;
			}

			.onoff input.toggle:checked + label:after {
				left: 20px;
				box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
			}
			.xx{
				float: right;
				background: #ccc;
				border-radius: 200px;
				width:14px;
				height: 13px;
				color: white;
				text-align: center;
				font-size: 10px;
			}
			.xx:hover{
				background: #777;
				cursor: pointer
			}
			.dataTables_wrapper .dataTables_filter input{
				border-radius: 10px;
				border: 1px solid #ccc;
				outline-style: none;
			}
</style>
   <div class="container-fluid">



          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3"><button  style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddPagarConta" >Adicionar</button>
			<div class="form-row">
              		<div class="col"><h4 class="m-0 font-weight-bold text-primary">Contas a Pagar</h4></div>
			  	
					  <div class="col-3"><input type="date" id="filtro-data-pagar-1" class="form-control" /></div>
					  <span style="align-self: center;">até</span>
					  <div class="col-3"><input type="date" id="filtro-data-pagar-2" class="form-control" /></div>
					  <div class="col-2"><button  style="float: right;margin-left: 10px" class=" btn btn-success" onclick="buscarPagar()" >Buscar</button></div>
				    </div>
            </div>
		  </div>
			
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTablePagar" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Vencimento</th>
                      <th>Valor</th>
                      <th>Fornecedor</th>
                      <th>Descricao</th>
                      <th width="10%">Pagar/Anular</th>
                    </tr>
                  </thead>
                  <tbody>
						<?php
							$total = 0;
						while($row = mysqli_fetch_array($res)) { 
                            $responsavel = $row['nome'];
                            $total += $row['valor'];
							?>
							<tr>
								<td><?php echo date('d/m/Y',strtotime($row['vencimento']));?></td>
                                <td><?php echo "R$ ".number_format($row['valor'], 2, ',','.');?></td>
                                <td><?php echo $responsavel;?></td>
                                <td><?php echo $row['descricao'];?></td>
								<?php if($row['status'] == 0){ ?>
								<td>
									<center>
										<button class="btn btn-primary btn-circle" onclick="pagar(<?php echo $row['id'];?>)" ><i class="fas fa-download" ></i></button>
										<button class="btn btn-danger btn-circle" onclick="cancel(<?php echo $row['id'];?>)" ><i class="fas fa-window-close" ></i></button>
									</center>
								</td>
								<?php }else if ($row['status']==1) {
								?>
								<td>
									<center>
										Paga
									</center>
								</td>
								<?php 
								}else if ($row['status']==2) {
								 ?>
								<td>
									<center>
										Anulada
									</center>
								</td>
								<?php }?>
							</tr>
						<?php }?>	
                  </tbody>
				  <tfoot>
                    <tr>
                      <th>Vencimento</th>
                      <th><?php echo "R$ ".number_format($total, 2, ',', '.');?></th>
                      <th>Fornecedor</th>
                      <th>Descricao</th>
                      <th width="10%">Pagar/Anular</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>
	
		<!-- PagarConta -->
        <div class="modal fade" id="PagarConta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Pagar Conta</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/pagar_conta.php" method="POST" enctype="multipart/form-data"  >
						<input id="id_pagar" name="id_pagar" type="hidden"  />
						<div class="form-row">
							<div class="col">
								<label style="color: grey;">Insira o comprovante</label><br>
                        		<input type="file" name="arquivo" id="arquivo" class="form-control"><br>
							</div>
						</div>
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
		

        	
		<!-- AddPagarConta -->
        <div class="modal fade" id="AddPagarConta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Pagar Conta</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_pagar_conta.php" method="POST" enctype="multipart/form-data"  >
                        <select name="fornecedor_pagar" id="fornecedor_pagar" class="form-control">
                        <option value="">Selecione um Fornecedor</option>
                        <?php 
                            $sql = "select * from fornecedor";
                            $res = mysqli_query($conn,$sql);
                            while($row = mysqli_fetch_array($res)){ ?>
                            <option value="<?php echo $row['id'];?>" ><?php echo $row['nome'];?></option>
                        <?php }?>    
                        </select><br>
                        <div class="form-row">
                            <div class="col-8"><input type="number" step="0.01" placeholder="Valor" name="valor_pagar" class="form-control" ><br></div>
                            <div class="col-4"><input type="number" step="1" placeholder="Qtd Vezes" name="vezes_pagar" class="form-control" ><br></div>
                        </div>
                        <label style="color: grey;">Data de vencimento:</label>
                        <input type="date" class="form-control" name="vencimento_pagar" ><br>
						<textarea class="form-control" name="descricao" placeholder="Descricão"></textarea><br>
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>


		<script>
			$(document).ready(function() {
                $('#dataTable').DataTable( {
                });
            });
                    
			function pagar(id_pagar){
                $('#id_pagar').val(id_pagar);
				$('#PagarConta').modal('show');
			}

			function cancel(id_pagar){
				var resp = confirm("Deseja anular essa conta ?");
  
				if(resp == true){
					$.get( "php/cancela_conta.php?id_pagar="+id_pagar, function( data ) {
		                location.reload();
					});
				}
			}

			function buscarPagar(){
				
				var data1 = $("#filtro-data-pagar-1").val();
				var data2 = $("#filtro-data-pagar-2").val();

				$.get( "php/filtro_data_pagar.php?ini="+data1+"&fim="+data2, function( data ) {
				     $("#dataTablePagar").html(data);
				});
			}
		</script>