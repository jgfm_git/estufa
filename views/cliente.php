<?php 
session_start();

require_once("../conn/conexao.php");


if(!empty($_SESSION['ZWxldHJpY2Ft'])){
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
}else{
	header('Location: login.php');
}


$sql = "select * from cliente
		";
$res = mysqli_query($conn,$sql);



	
?>   
<style>
.onoff input.toggle {
				display: none;
			}

			.onoff input.toggle + label {
				display: inline-block;
				position: relative;
				box-shadow: inset 0 0 0px 1px #d5d5d5;
				height: 20px;
				width: 40px;
				border-radius: 30px;
			}

			.onoff input.toggle + label:before {
				content: "";
				display: block;
				height: 20px;
				width: 40px;
				border-radius: 30px;
				background: rgba(19, 191, 17, 0);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle + label:after {
				content: "";
				position: absolute;
				height: 20px;
				width: 20px;
				top: 0;
				left: 0px;
				border-radius: 30px;
				background: #fff;
				box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle:checked + label:before {
				width: 40px;
				background: #13bf11;
			}

			.onoff input.toggle:checked + label:after {
				left: 20px;
				box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
			}
			.xx{
				float: right;
				background: #ccc;
				border-radius: 200px;
				width:14px;
				height: 13px;
				color: white;
				text-align: center;
				font-size: 10px;
			}
			.xx:hover{
				background: #777;
				cursor: pointer
			}
			.dataTables_wrapper .dataTables_filter input{
				border-radius: 10px;
				border: 1px solid #ccc;
				outline-style: none;
			}
</style>
   <div class="container-fluid">



          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h4 class="m-0 font-weight-bold text-primary">Clientes
				<button  style="float: right;margin-left: 10px" class=" btn btn-success" onclick="geraClienteIndex()" >Adicionar</button>
				
			  </h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>CNPJ</th>
                      <th>Razão Social</th>
                      <th>Endereço</th>
                      <th>Telefone</th>
                      <th>Barraca</th>
                      <th>Status</th>
                      <th width="10%">Editar</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>CNPJ</th>
                      <th>Razão Social</th>
                      <th>Endereço</th>
                      <th>Telefone</th>
                      <th>Barraca</th>
                      <th>Status</th>
                      <th width="10%">Editar</th>
                    </tr>
                  </tfoot>
                  <tbody>
						<?php
							$x =0;
						while($row = mysqli_fetch_array($res)) { 
                            $checked = "";
                            if($row['status'] == 1){
                                $checked = "checked";
                            }
                            if (is_null($row['barraca'])) {
                            	$barraca = "Não cadastrada";
                            }else{
                            	$barraca = $row['barraca'];
                            }
							?>
							<tr>
								<td><?php echo $row['cnpj'];?></td>
								<td><?php echo $row['razao_social'];?></td>
                                <td><?php echo $row['endereco'];?></td>
								<td><?php echo $row['telefone'];?></td>
								<td><?php echo $barraca; ?></td>
								<td><div class="onoff">
									<input type="checkbox" class="toggle" id="onoff<?php echo $row['id'];?>" onchange="altera_status_cli(<?php echo $row['id'];?>)" <?php echo $checked;?>>
									<label for="onoff<?php echo $row['id'];?>"></label>
									<input id="estado<?php echo $row['id'];?>" style="display:none" value="<?php echo $status_on_off;?>" />
								</div></td>
								<td><center><button class="btn btn-warning btn-circle" onclick="edit(<?php echo $row['id'];?>)" ><i class="fas fa-edit" ></i></button></center></td>
							</tr>
						<?php }?>	
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
	
		  
		  	<!-- EditProd -->
		 <div class="modal fade" id="EditCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document" style="max-width: 1000px;">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar Cliente</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
					</div>
					<div class="modal-body">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Dados</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Financeiro</a>
							</li>
						<!--	<li class="nav-item">
								<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"></a>
							</li>-->
						</ul>
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
									
								<div id="conteudo-dados">
								
								</div>
							</div>
							<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
								<div id="conteudo-financeiro"></div>
							</div>
							<!--<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>-->
						</div>
					</div>
				  </div>
			</div>
		  </div>
        <!-- /.container-fluid -->
		
		
		<script>
			$(document).ready(function() {
				$('#dataTable').DataTable( {
				});
			});
			
		function geraClienteIndex(){
			$("#id_orcamento_cli").val(0);
			$("#razao").val("");
			$("#responsavel").val("");
			$("#AddCli").modal('show');
		}

		function edit(id){
			var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 50%;margin-top: 10%;margin-bottom: 10%'><span class='sr-only'>Loading...</span></div>";
			$("#conteudo-dados").html(data);
			$("#conteudo-financeiro").html(data);

			$.get( "php/dados_cliente.php?id="+id, function( data ) {
				$("#conteudo-dados").html(data);
			});

			$.get( "php/financeiro_cliente.php?id="+id, function( data ) {
				$("#conteudo-financeiro").html(data);
			});

			$('#EditCliente').modal('show');
		}

		function altera_status_cli(id){
		if($('#onoff'+id).is(':checked')){
			var status = 1;
			$.get("php/altera_status_cliente.php?id_cliente="+id+"&status="+status, function (data){
				
			});
		}else{
			var status = 0;
			$.get("php/altera_status_cliente.php?id_cliente="+id+"&status="+status, function (data){
				
			});
		}
	}
		</script>
		