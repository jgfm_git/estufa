<?php
error_reporting(0);
include_once("../conn/conexao.php");
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo'); 

$tipo_responsavel = $_GET['tipo'];
$id_responsavel = $_GET['id'];
$id_nota = $_GET['id_nota'];
$view = $_GET['view'];

if ($tipo_responsavel == "Fornecedor") {
    $sql = "
        select
            e.lote,
            e.quantidade,
            e.valor,
            n.valor_total as valor_total,
            f.nome,
            f.cnpj,
            f.telefone,
            f.endereco,
            f.bairro,
            f.numero,
            f.cidade,
            s.descricao
        from entrada as e
            inner join fornecedor as f on
            e.id_responsavel = f.id
            inner join nota_entrada as n on
            e.id_nota_entrada = n.id
            inner join semente as s on
            e.id_semente = s.id
        where e.id_nota_entrada = $id_nota
        ";
}else if($tipo_responsavel == "Cliente"){
    $sql = "
        select
            e.lote,
            e.quantidade,
            e.valor,
            n.valor_total as valor_total,
            c.razao_social as nome,
            c.cnpj,
            c.telefone,
            c.endereco,
            c.bairro,
            c.numero,
            c.cidade,
            s.descricao
        from entrada as e
            inner join cliente as c on
            e.id_responsavel = c.id
            inner join nota_entrada as n on
            e.id_nota_entrada = n.id
            inner join semente as s on
            e.id_semente = s.id
        where e.id_nota_entrada = $id_nota
        ";
}

$res = mysqli_query($conn,$sql);

$descricao = array();
$quantidade = array();
$lote = array();
$valor_prod = array();
$qtd_prod = 0;

while($row = mysqli_fetch_array($res)){
    $valor    = $row['valor_total'];
    array_push($valor_prod,$row['valor']);
    array_push($quantidade,$row['quantidade']);
    array_push($lote,$row['lote']);
    array_push($descricao,$row['descricao']);
    $nome  = $row['nome'];
    $cnpj  = $row['cnpj'];
    $telefone  = $row['telefone'];
    $endereco  = $row['endereco'];
    $numero  = $row['numero'];
    $bairro  = $row['bairro'];
    $cidade  = $row['cidade'];

    $qtd_prod ++;
}

?>



<!DOCTYPE html>
<html>
<head>


  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Via do Responsável | Estufa</title>

  <!-- Custom fonts for this template-->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../css/sb-admin-2.min.css" rel="stylesheet">
  <link href="img/logo_seco.png" rel="shortcut icon">
 
 <style>
 .header{
    margin: 20px
 }
 label{
    display: block;
    margin-bottom: 0rem;
    font-size: 1.5rem;
 }
 .divider{
     position: relative;
     height: 1px;
     width: 100%;
     background: #101010;
     margin-top: 15px;
 }
 body{
     color: #000;
 }
 .assinatura{
    text-align: center;
    margin-top: 10%;
 }

 </style>
</head>
<body>
    <div class="header" >
        <div class="form-row">
            <div class="col">
                <img src="../img/eletricam.png" width="100%" />
            </div>
            <div class="col text-right">
                <label style="font-size:1.31rem;">Viveiro de mudas Bajú e Sergio, Ribeirão Branco - SP</label>
                <label style="font-size:1.4rem;">viveirobajuesergio.rb@gmail.com</label>
                <label style="font-size:1.4rem;">(15) 99777-7582 / 99648-9875 / 99615-2264</label>
            </div>
        </div>
    </div>
    <br><br>
    <label class="divider"></label>
    <br>
    <div class="descricao">
        <div class="form-row">
            <label style="position: absolute; padding-left:700px;"><b>Data de emissão:     </b><span><?php echo date('d/m/Y',strtotime($data_cad));?></span></label><br>
        </div>
        <br><br>
        <div class="form-row">
            <div class="col">
                <label><b>Dados</b></label>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <label><b>Nome:             </b><span><?php echo $nome;?></span></label>
                <label><b>CNPJ:             </b><span><?php echo $cnpj;?></span></label>
                <label><b>Telefone:         </b><span><?php echo $telefone;?></span></label>
            </div>
            <div class="col">
                <label><b>Endereço: </b><?= $endereco." nº ".$numero.", ".$bairro." - ".$cidade ?></label>
            </div>
        </div>
    </div>
    <label class="divider"></label>
    <br>
    <div class="servicos_produtos">
    <div class="form-row">
            <div class="col-6">
                <label><b>Descrição</b></label>
            </div>
            <div class="col-2">
                <label><b>Lote</b></label>
            </div>
            <div class="col-2">
                <label><b>Quantidade</b></label>
            </div>
            <div class="col-2">
                <label><b>Valor</b></label>
            </div>
        </div>
        
        <div class="form-row">
        <div class="col-6">
                <?php for($i=0; $i < $qtd_prod; $i++){ ?>
                <label><?=$descricao[$i];?></label>
                <?php } ?>
            </div>
            <div class="col-2">
                <?php for($i=0; $i < $qtd_prod; $i++){ ?>
                    <label><?= $lote[$i];?></label>
                <?php } ?>
            </div>
            <div class="col-2">
                <?php for($i=0; $i < $qtd_prod; $i++){ ?>
                    <label><?= number_format($quantidade[$i],0,'','.');?></label>
                <?php } ?>
            </div>
            <div class="col-2">
                <?php for($i=0; $i < $qtd_prod; $i++){ ?>
                    <label><?= "R$ ".number_format($valor_prod[$i],2,',','.');?></label>
                <?php } ?>
            </div>
        </div>
        <?php
        ?>
        <br>
        <div class="form-row">
            <div class="col-8">
                <label><b>Total Geral</b></label>
            </div>
            <div class="col-2">
                <label><b></b></label>
            </div>
            <div class="col-2">
                <label><b>R$ <?php echo number_format($valor,2,',','.');?></b></label>
            </div>
        </div>
    </div>
    <br>
    <label class="divider"></label>
    <div class="assinatura">
        <label>___________________________________</label>
        <label><b>Responsável</b></label>
    </div>

</body>
<?php if ($view==0) { ?>
<script>window.print();</script>
<?php } ?>
<script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../js/sb-admin-2.min.js"></script>
</html>