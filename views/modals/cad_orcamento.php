	 <?php

      $sql = "select * from semente";
      $resProduto = mysqli_query($conn,$sql);


      $sql = "select * from pagamento";
      $resPagamento = mysqli_query($conn,$sql);
      
      
      ?>
     
     
         <!-- AddOrcamento -->
         <div class="modal fade" id="AddOrc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Novo Orçamento</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
					</div>
					<div class="modal-body">
						<form action="php/cadastra_orcamento.php" method="POST" enctype="multipart/form-data" >
                          <span>Cliente? | </span>
                          <input name="collapseGroup" onchange="cliente(this)" type="radio" data-toggle="collapse" data-target="#collapseOne" value="1" checked/> Sim
                          <input name="collapseGroup" onchange="cliente(this)" type="radio" data-toggle="collapse" data-target="#collapseOne" value="0" /> Não
                          <br>
                    
                          <div id="cliente_radio_nao" class="hide">
                            <br>
                            <div class="form-row">
                                <div class="col">
                                    <input id="cliente_cad_orc" name="cliente_cad_orc" type="text" class="form-control" placeholder="Nome do cliente"/></br>
                                </div>
                                <div class="col">
                                    <input type="text" name="cliente_cad_cnpj" id="cliente_cad_cnpj" class="form-control" placeholder="CNPJ/CPF">
                                </div>
                            </div>
                            <div class="form-row">
                              <div class="col">
                                <span>Data de nascimento</span>
                                <input type="date" name="cliente_cad_data_nasc" id="cliente_cad_data_nasc" class="form-control">
                              </div>
                              <div class="col" style="padding-top: 20px;">
                                <input type="text" name="cliente_cad_telefone" id="cliente_cad_telefone" class="form-control" placeholder="Telefone">
                              </div>
                            </div><br>
                            <div class="form-row">
                              <div class="col">
                                  <input type="text" name="cliente_cad_endereco" class="form-control" placeholder="Endereço completo">
                              </div>
                              <div class="col">
                                <input type="text" name="cliente_cad_cidade" class="form-control" placeholder="Cidade"><br>
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="col">
                                <textarea name="cliente_cad_obs" id="cliente_cad_obs" class="form-control" placeholder="Observacao sobre o cliente"></textarea>
                              </div>
                            </div>
                          </div><br>
                          <div id="cliente_radio_sim" class="show">
                            <div class="form-row">
                                <div class="col">
                                    <input id="cliente_orc" name="cliente_orc" type="text" class="form-control" placeholder="Nome do cliente"/></br>
                                </div>
                            </div>
                          </div>

                           <input type="hidden" id="qtd_produto" name="qtd_produto" value="1" />
                            <div id="div-produtos">
                                <div class="form-row">
                                    <div class="col-8">
                                        <select  name="produto_orc_0" class="form-control">
                                            <option >Selecione um Produto</option>
                                            <?php 
                                            while($row = mysqli_fetch_array($resProduto)){ ?> 
                                                <option value="<?php echo $row['id'];?>"><?php echo $row['descricao'];?></option>
                                            <?php }?>
                                        </select></br>
                                    </div>
                                    <div class="col-2">
                                        <input name="qtd_prod_0" type="number" step="1" class="form-control" placeholder="Qtd" />
                                    </div>
                                    <div class="col-2">
                                    <button class="btn btn-success" type="button" onclick="novo_produto()" ><i class="fas fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                              <div class="col">
                                <span>Data de entrega</span>
                                <input type="date" name="data_entrega" id="data_entrega" class="form-control">
                              </div>
                              <div class="col">
                                <span>Forma de pagamento</span>
                                  <select name="pagamento_orc" id="pagamento_orc" class="form-control" >
                                      <option value="">Selecione um Pagamento</option>
                                  <?php while($row = mysqli_fetch_array($resPagamento)){?>
                                      <option value="<?php echo $row['id'];?>"><?php echo $row['nome'];?></option>
                                  <?php }?>    
                                  </select>    
                              </div>
                           
                            </div><br>
                            <div class="form-row">
                              <div class="col">
                                <textarea name="observacao" id="observacao" class="form-control" placeholder="Observação sobre o orçamento"></textarea>
                              </div>
                            </div><br>
							<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
							<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
						</form>
					</div>
				  </div>
			</div>
		  </div>

          <script>
         
            $(function() {
                $.get( "php/get_clientes.php", function( data ) {
                    var clientes = JSON.parse(data);
                
                    $("#cliente_orc" ).autocomplete({
                        source: clientes
                    });
                });
            });




            function novo_produto(){
               var div_row = document.createElement("div");
               div_row.setAttribute('class','form-row');
               div_row.setAttribute('id','div-produto-'+quantidade_produto);
               
               
               $.get( "php/get_produto_orc.php?id="+quantidade_produto, function( data ) {
                    div_row.innerHTML = data;
                    quantidade_produto++;
                    document.getElementById('qtd_produto').value = quantidade_produto;
               });


               document.getElementById('div-produtos').appendChild(div_row);
               
               
            }

            function remove_produto(id){
                var div = document.getElementById('div-produto-'+id);
                div.parentNode.removeChild(div);
                quantidade_produto--;
                document.getElementById('qtd_produto').value = quantidade_produto;
            }

            function cliente(obj){
              if (obj.value == 0) {
                $('#cliente_radio_nao').addClass("show");
                $('#cliente_radio_nao').removeClass("hide");

                $('#cliente_radio_sim').removeClass("show");
                $('#cliente_radio_sim').addClass("hide");

                $('#cliente_orc').val("");

              }else{
                $('#cliente_radio_nao').removeClass("show");
                $('#cliente_radio_nao').addClass("hide");

                $('#cliente_radio_sim').removeClass("hide");
                $('#cliente_radio_sim').addClass("show");

                $('#cliente_cad_orc').val("");
                $('#cliente_cad_cnpj').val("");
    
              }
            }


          </script>