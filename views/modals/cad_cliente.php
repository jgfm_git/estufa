	<!-- AddProd -->
    <div class="modal fade" id="AddCli" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro de Cliente</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_cliente.php" method="POST" id="form-cli" >
                        <input type="hidden" id="id_orcamento_cli" name="id_orcamento_cli">
                            
                        <div class="form-row">
                            <div class="col">
                                <label style="color: grey;">Nome</label>
                                <input name="razao" id="razao" type="text" placeholder="Razão Social / Nome Completo" class="form-control" required/><br>
                            </div>
                            <div class="col">
                                <label style="color: grey;">Data de nascimento</label>
                                <input type="date" name="data_nasc" id="data_nasc" class="form-control">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input name="pai" id="pai" type="text" placeholder="Nome do pai" class="form-control" requied/><br>
                            </div>
                            <div class="col">
                                <input name="mae" id="mae" type="text" placeholder="Nome da mãe" class="form-control" requied/><br>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col">
                                <input name="cnpj" type="text" placeholder="CNPJ / CPF" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input name="rg" type="text" placeholder="RG" class="form-control" /><br>
                            </div>
                            <div class="col">
                                <input type="email" name="email" id="email" placeholder="Email" class="form-control"><br>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input name="responsavel" id="responsavel" type="text" placeholder="Responsavel" class="form-control" required/><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-4">
                                <input id="cep" name="cep" type="text" placeholder="CEP" class="form-control" required/><br>
                            </div>
                            <div class="col">
                                <input value="Buscar Cep" id="buscaCep" type="button" placeholder="Cep" class="btn btn-primary" /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-8">
                                <input id="endereco" name="endereco" type="text" placeholder="Endereço completo" class="form-control" required/><br>
                            </div>
                            <div class="col-4">
                                <input name="numero" type="text" placeholder="Numero" class="form-control" required/><br>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col">
                                <input id="bairro" name="bairro" type="text" placeholder="Bairro" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input id="cidade" name="cidade" type="text" placeholder="Cidade" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <select class="form-control" name="barraca" id="barraca">
                                    <option value="">Selecione uma barraca</option>
                                    <?php 
                                        $sql = "SELECT * FROM barraca";
                                        $res = mysqli_query($conn,$sql);
                                        while ($row = mysqli_fetch_array($res)) {
                                    ?>
                                    <option value="<?= $row['nome'] ?>"><?= $row['nome'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input name="telefone" type="text" placeholder="Telefone" class="form-control" required/>
                            </div>
                            <div class="col">
                                <input name="telefone2" type="text" placeholder="Telefone 2 (Opcional)" class="form-control" />
                            </div>
                        </div></br>         
                        <textarea name="observacao" type="text" placeholder="Digite uma observação" class="form-control" ></textarea><br>
						
						
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
        
        <script>
        $(document).ready(function () {
            $("#buscaCep").click(function(){

            //Nova variável "cep" somente com dígitos.
            var cep = $("#cep").val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco").val(dados.logradouro);
                            $("#endereco").css("background","#eee");
                            $("#bairro").val(dados.bairro);
                            $("#bairro").css("background","#eee");
                            $("#cidade").val(dados.localidade);
                            $("#cidade").css("background","#eee");
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            });
        });



       
        </script>