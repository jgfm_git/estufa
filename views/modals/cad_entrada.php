<?php

$sql = "select * from semente";
$resSemente = mysqli_query($conn,$sql);

$sql = "select * from fornecedor";
$resFornecedor = mysqli_query($conn,$sql);

$sql = "select * from cliente";
$resCliente = mysqli_query($conn,$sql);

?>


   <!-- AddOrcamento -->
   <div class="modal fade" id="AddEstoque" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Nova Entrada</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
              </div>
              <div class="modal-body">
                  <form action="php/cadastra_entrada.php" method="POST" enctype="multipart/form-data" >
                  <span>Cliente ou Fornecedor? | </span>
                          <input name="collapseEntrada" onchange="entrada(this)" type="radio" value="1" checked/> Fornecedor
                          <input name="collapseEntrada" onchange="entrada(this)" type="radio" value="0" /> Cliente
                          <br>
                    
                          <div id="entrada_radio_nao" class="hide">
                          <br>
                            <div class="form-row">
                                <div class="col">
                                    <select name="cliente" id="cliente" class="form-control">
                                        <option value="">Selecione um cliente</option>
                                        <?php while($row = mysqli_fetch_array($resCliente)){ ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['razao_social'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div><br>
                          </div>
                          <div id="entrada_radio_sim" class="show">
                          <br>
                            <div class="form-row">
                                <div class="col">
                                    <select name="fornecedor" id="fornecedor" class="form-control">
                                        <option value="">Selecione um fornecedor</option>
                                        <?php while($row = mysqli_fetch_array($resFornecedor)){ ?>
                                        <option value="<?= $row['id'] ?>"><?= $row['nome'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div><br>
                          </div>

                           <input type="hidden" id="qtd_produto_estoque" name="qtd_produto_estoque" value="1" />
                            <div id="div-produtos-estoque">
                                <div class="form-row">
                                    <div class="col-5">
                                        <select  name="produto_estoque_0" id="produto_estoque_0">
                                            <option>Selecione a variedade</option>
                                            <?php 
                                            while($row = mysqli_fetch_array($resSemente)){ ?> 
                                                <option value="<?php echo $row['id'];?>"><?php echo $row['descricao'];?></option>
                                            <?php }?>
                                        </select></br>
                                    </div>
                                    <div class="col-2">
                                        <input name="qtd_prod_estoque_0" type="number" step="1" class="form-control" placeholder="Qtd" />
                                    </div>
                                    <div class="col-2">
                                        <input type="number" step="0.01" name="valor_estoque_0" id="valor_estoque_0" class="form-control" placeholder="Preço" required>
                                    </div>
                                    <div class="col-2">
                                        <input type="text" name="lote_estoque_0" id="lote_estoque_0" class="form-control" placeholder="Lote" required>
                                    </div>
                                    <div class="col-1">
                                    <button class="btn btn-success" type="button" onclick="novo_prod()" ><i class="fas fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>
                        <div class="form-row">
                            
                            
                        </div><br>
                      <button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
                      <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
                  </form>
              </div>
            </div>
      </div>
    </div>

<script>
    var quantidade_produto_estoque = 1;
    var indice = 0;
    function novo_prod(){
               var div_row_estoque = document.createElement("div");
               div_row_estoque.setAttribute('class','form-row');
               div_row_estoque.setAttribute('id','div-produto-estoque-'+quantidade_produto_estoque);
               
               
               $.get( "php/get_produto_estoque.php?id="+quantidade_produto_estoque, function( data ) {
                    div_row_estoque.innerHTML = data;
                    quantidade_produto_estoque++;
                    document.getElementById('qtd_produto_estoque').value = quantidade_produto_estoque;
                    indice++;
               });


               document.getElementById('div-produtos-estoque').appendChild(div_row_estoque);
               
               setTimeout(() => {
                  $('#produto_estoque_'+indice).selectize({
                      create: true,
                      sortField: 'text'
                  });
                  
                }, 100);
            }

            function remove_prod(id){
                var div_estoque = document.getElementById('div-produto-estoque-'+id);
                div_estoque.parentNode.removeChild(div_estoque);
                quantidade_produto_estoque--;
                document.getElementById('qtd_produto_estoque').value = quantidade_produto_estoque;
                indice--;
            }

            function entrada(obj){
              if (obj.value == 0) {
                $('#entrada_radio_nao').addClass("show");
                $('#entrada_radio_nao').removeClass("hide");

                $('#entrada_radio_sim').removeClass("show");
                $('#entrada_radio_sim').addClass("hide");

                $('#cliente').val("");

              }else{
                $('#entrada_radio_nao').removeClass("show");
                $('#entrada_radio_nao').addClass("hide");

                $('#entrada_radio_sim').removeClass("hide");
                $('#entrada_radio_sim').addClass("show");

                $('#fornecedor').val("");
              }
            }
            // var countVariedade = $('#qtd_produto_estoque').val
            // for (var i = 0; i < countVariedade; i++) {
            //   $('#produto_estoque_'+countVariedade).selectize({
            //     create: true,
            //     sortField: 'text'
            //   });
            // }

            $('#produto_estoque_0').selectize({
                create: true,
                sortField: 'text'
            });
            
</script>