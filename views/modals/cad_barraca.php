	<!-- AddProd -->
    <div class="modal fade" id="AddBarraca" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro de Barraca</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_barraca.php" method="POST"  >
                        <div class="form-row">
                            <div class="col">
                                <input type="text" name="nome_barraca" id="nome_barraca" class="form-control" placeholder="Nome da Barraca"><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <input type="text" name="telefone_barraca" id="telefone_barraca" class="form-control" placeholder="Telefone">
                            </div>
                            <div class="col">
                                <input type="text" name="email_barraca" id="email_barraca" class="form-control" placeholder="Email">
                            </div>
                        </div><br>
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
    