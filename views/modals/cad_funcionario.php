	<!-- AddProd -->
    <div class="modal fade" id="AddFunc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro de Funcionario</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/cadastra_funcionario.php" method="POST"  >
                        <div class="form-row">
                            <div class="col">
                                <input name="nome_func" type="text" placeholder="Nome do Funcionario" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input name="cpf_func" id="cpf_func" type="text" placeholder="CPF / CNPJ" class="form-control"  required/><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <input name="cnh_func" type="text" placeholder="CNH" class="form-control"  /><br>
                            </div>
                            <div class="col">
                                <input name="validade_func" id="validade_func" type="date"  class="form-control"  /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-4">
                                <input id="cep_func" name="cep_func" type="text" placeholder="CEP" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input value="Buscar Cep" id="buscaCepFunc" type="button"  class="btn btn-primary" /><br>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-8">
                                <input id="endereco_func" name="endereco_func" type="text" placeholder="Endereço completo" class="form-control" required/><br>
                            </div>
                            <div class="col-4">
                                <input id="numero_func" name="numero_func" type="text" placeholder="Numero" class="form-control" required/><br>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col">
                                <input id="bairro_func" name="bairro_func" type="text" placeholder="Bairro" class="form-control" required /><br>
                            </div>
                            <div class="col">
                                <input id="cidade_func" name="cidade_func" type="text" placeholder="Cidade" class="form-control" required /><br>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col">
                                <input name="telefone_func" type="text" placeholder="Telefone" class="form-control" required />
                            </div>
                        </div><br>
                                
						
						<button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
						<button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
        
        <script>
        $(document).ready(function () {
            $("#buscaCepFunc").click(function(){

            //Nova variável "cep" somente com dígitos.
            var cep = $("#cep_func").val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco_func").val(dados.logradouro);
                            $("#endereco_func").css("background","#eee");
                            $("#bairro_func").val(dados.bairro);
                            $("#bairro_func").css("background","#eee");
                            $("#cidade_func").val(dados.localidade);
                            $("#cidade_func").css("background","#eee");
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            });
        });
        </script>