<div class="modal fade" id="AddProd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Cadastro de Semente</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
        </div>
            <div class="modal-body">
                <form action="php/cadastra_produto.php" method="POST"  >
                    <input type="hidden" id="id_produto" name="id_produto" >
                    <div class="form-row">
                        <div class="col">
                            <input name="descricao" id="descricao" type="text" placeholder="Variedade da semente" class="form-control" required /><br>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <input name="preco" id="preco" type="number" step="0.01" placeholder="Preço" class="form-control"  required /><br>
                        </div>
                        <div class="col">
                            <input name="quantidade" id="quantidade" type="number" step="1" placeholder="Quantidade" class="form-control"  required /><br>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <textarea name="obs" id="obs" type="text" placeholder="Observação" class="form-control"  ></textarea><br>
                        </div>
                    </div>
                    <button class="btn btn-success" type="submit" style="float: right">Cadastrar</button>
                    <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
                </form>
            </div>
        </div>
    </div>
</div>
