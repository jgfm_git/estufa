<?php 
session_start();

require_once("../conn/conexao.php");

function getResponsavel($tipo,$id){
  global $conn;
  if ($tipo=="Fornecedor") {
    $sql = "SELECT nome FROM fornecedor WHERE id=$id";
    $res = mysqli_query($conn,$sql);
  }elseif($tipo=="Cliente"){
    $sql = "SELECT razao_social FROM cliente WHERE id=$id";
    $res = mysqli_query($conn,$sql);
  }
  while($row = mysqli_fetch_array($res)){
    $nome = $row[0];
  }
  return $nome;
}


$sql = "select * from semente";
$resSemente = mysqli_query($conn,$sql);

$sql = "select * from fornecedor";
$resFornecedor = mysqli_query($conn,$sql);

$sql = "select * from pagamento";
$resPagamento = mysqli_query($conn,$sql);

if(!empty($_SESSION['ZWxldHJpY2Ft'])){
	$usuario_id = $_SESSION['ZWxldHJpY2Ft'];
}else{
	header('Location: login.php');
}


$sql = "
        SELECT 
          n.valor_total,
          n.id,
          e.valor,
          e.tipo_responsavel,
          e.id_responsavel,
          e.status
        FROM 
          nota_entrada as n 
          INNER JOIN entrada as e on 
          n.id = e.id_nota_entrada 
        GROUP BY e.id_nota_entrada
        ";
$res = mysqli_query($conn,$sql);

?>   
<style>
.onoff input.toggle {
				display: none;
			}

			.onoff input.toggle + label {
				display: inline-block;
				position: relative;
				box-shadow: inset 0 0 0px 1px #d5d5d5;
				height: 20px;
				width: 40px;
				border-radius: 30px;
			}

			.onoff input.toggle + label:before {
				content: "";
				display: block;
				height: 20px;
				width: 40px;
				border-radius: 30px;
				background: rgba(19, 191, 17, 0);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle + label:after {
				content: "";
				position: absolute;
				height: 20px;
				width: 20px;
				top: 0;
				left: 0px;
				border-radius: 30px;
				background: #fff;
				box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.2), 0 2px 4px rgba(0, 0, 0, 0.2);
				transition: 0.1s ease-in-out;
			}

			.onoff input.toggle:checked + label:before {
				width: 40px;
				background: #13bf11;
			}

			.onoff input.toggle:checked + label:after {
				left: 20px;
				box-shadow: inset 0 0 0 1px #13bf11, 0 2px 4px rgba(0, 0, 0, 0.2);
			}
			.xx{
				float: right;
				background: #ccc;
				border-radius: 200px;
				width:14px;
				height: 13px;
				color: white;
				text-align: center;
				font-size: 10px;
			}
			.xx:hover{
				background: #777;
				cursor: pointer
			}
			.dataTables_wrapper .dataTables_filter input{
				border-radius: 10px;
				border: 1px solid #ccc;
				outline-style: none;
			}
      .show{
				display: block;
			}
			.hide{
				display: none;
			}
</style>
   <div class="container-fluid">

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h4 class="m-0 font-weight-bold text-primary">Entradas
				<button  style="float: right;margin-left: 10px" class=" btn btn-success" data-toggle="modal" data-target="#AddEstoque" >Adicionar</button>
				
			  </h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th width="5%">Via do Fornecedor</th>
                      <th>Fornecedor/Cliente</th>
                      <th>Valor total</th>
                      <th width="10%">Tipo de Responsável</th>
                      <th width="9%">Conta/Dar baixa</th>
                      <th width="8%">Editar</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th width="5%">Via do Fornecedor</th>
                      <th>Fornecedor/Cliente</th>
                      <th>Valor total</th>
                      <th width="10%">Tipo de Responsável</th>
                      <th width="9%">Conta/Dar baixa</th>
                      <th width="8%">Editar</th>
                    </tr>
                  </tfoot>

                  <tbody>
                  <?php
                  while($row = mysqli_fetch_array($res)) {       
                    $tipo_responsavel = $row['tipo_responsavel'];

                  ?>  

							      <tr>
                      <td>
                          <center>
                              <a class="btn btn-primary btn-circle" target="_BLANK" 
                                href="views/print_via_responsavel.php?id=<?=$row['id_responsavel'];?>&tipo=<?=$tipo_responsavel?>&id_nota=<?=$row['id']?>&view=1">
                              <i class="fas fa-print"></i></a>
                          </center>
                      </td>
                      <td><?= getResponsavel($row['tipo_responsavel'],$row['id_responsavel']); ?></td>
                      <td><?= $row['valor_total']; ?></td>
                      <td><?= $row['tipo_responsavel']; ?></td>
                      <?php
                      if($row['status'] == 0){
                      ?>
                      <td>
                          <center>
                              <button class="btn btn-info btn-circle" onclick="gerarConta(<?=$row['id'];?>,'<?=$row['tipo_responsavel'];?>')"><i class="fas fa-receipt"></i></button>
                              <button class="btn btn-success btn-circle" onclick="baixaConta(<?=$row['id'];?>,'<?=$row['tipo_responsavel'];?>')"><i class="fas fa-money-bill-alt"></i></button>
                          </center>
                      </td>
                      <?php }elseif($row['status']==1){ ?>
                      <td>
                          <center>
                              Já cadastrada
                          </center>
                      </td>
                      <?php } ?>
							      	<td>
                        <center>
                            <button class="btn btn-warning btn-circle" onclick="edit_entrada(<?php echo $row['id'];?>)" ><i class="fas fa-edit" ></i></button>
                        </center>
                      </td>
							      </tr>
						      <?php }?>	
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>

			<div class="modal fade" id="EditEntrada" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:10%;">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Editar Estoque</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/edita_estoque.php" method="POST"  >
						<input type="hidden" id="id_estoque_edit" name="id_estoque_edit" >
                        <div class="form-row">
                            <div class="col">
                                <select name="fornecedor_edit" id="fornecedor_edit" class="form-control" required>
                                    <option value="">Selecione um fornecedor</option>
                                    <?php 
                                    while ($row = mysqli_fetch_array($resFornecedor)) {
                                    ?>
                                    <option value="<?= $row['id'] ?>"><?= $row['nome'] ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="form-row">
                            <div class="col">
                                <select name="semente_edit" id="semente_edit" class="form-control" required>
                                    <option value="">Variedade da semente</option>
                                    <?php 
                                    while ($row = mysqli_fetch_array($resSemente)) {
                                    ?>
                                    <option value="<?= $row['id'] ?>"><?= $row['descricao'] ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        <div class="form-row">
                            <div class="col">
                                <input type="number" name="quantidade_edit" id="quantidade_edit" class="form-control" placeholder="Quantidade" required>
                            </div>
                            <div class="col">
                                <input type="number" step="0.01" name="valor_edit" id="valor_edit" class="form-control" placeholder="Preço" required>
                            </div>
                            <div class="col">
                                <input type="text" name="lote_edit" id="lote_edit" class="form-control" placeholder="Lote da Variedade" required>
                            </div>
                        </div><br>
                    <button class="btn btn-success" type="submit" style="float: right">Alterar</button>
                    <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>

      <div class="modal fade" id="geraConta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:10%;">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Gerar conta a pagar</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/gera_conta.php" method="POST"  >
						<input type="hidden" id="id_estoque" name="id_estoque" >
                        <div class="form-row">
                            <div class="col">
                                <span>Data de vencimento da conta</span>
                                <input type="date" name="vencimento" id="vencimento" class="form-control">
                            </div>
                        </div><br>
                        <div class="form-row">
                            <div class="col">
                                <span>Forma de pagamento</span>
                                <select name="pagamento_id" id="pagamento_id" class="form-control">
                                    <option value="">Selecione uma forma de pagamento</option>
                                <?php
                                while ($row = mysqli_fetch_array($resPagamento)) {
                                    ?>
                                    <option value="<?= $row['id'] ?>"><?= $row['nome'] ?>
                                <?php } ?>
                                </select>
                            </div>
                        </div><br>
                        
                    <button class="btn btn-success" type="submit" style="float: right">Gerar</button>
                    <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>

          <div class="modal fade" id="baixaConta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:10%;">
			<div class="modal-dialog" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Dar baixa na conta</h5>
				  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				  </button>
				</div>
				<div class="modal-body">
					<form action="php/baixa_conta.php" method="POST"  >
						<input type="hidden" id="id_estoque_baixa" name="id_estoque_baixa" >
                        <div class="form-row">
                            <div class="col">
                                <span>Data de vencimento da conta (caso não seja na data atual)</span>
                                <input type="date" name="vencimento" id="vencimento" class="form-control">
                            </div>
                        </div><br>
                        
                    <button class="btn btn-success" type="submit" style="float: right">Dar baixa</button>
                    <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
					</form>
				</div>
				  </div>
			</div>
		  </div>
		
		<script>
			$(document).ready(function() {
                $('#dataTable').DataTable( {
                  "aaSorting": [[1,"desc"]]
                });
            });
                    
		
		function edit_entrada(id){
			$.get( "php/get_estoque.php?id="+id, function( data ) {
                var json = JSON.parse(data);
                $( "#id_estoque_edit" ).val( id );
                $( "#semente_edit" ).val( json[0].id_semente );
                $( "#quantidade_edit" ).val(json[1].quantidade);
                $( "#valor_edit" ).val( json[2].valor );
                $( "#fornecedor_edit" ).val( json[3].id_fornecedor );
                $( "#lote_edit" ).val(json[4].lote)
                $('#EditEntrada').modal('show');
			});
        }

        function gerarConta(id,pessoa){
            if (pessoa == 'Fornecedor') {
              $('#id_estoque').val(id);
              $('#geraConta').modal('show');
            }else{
              alert("Em criação");
            }
        }

        function baixaConta(id){
            $('#id_estoque_baixa').val(id);
            $('#baixaConta').modal('show');
        }

        function entrada(obj){
          if (obj.value == 0) {
            $('#entrada_radio_nao').addClass("show");
            $('#entrada_radio_nao').removeClass("hide");

            $('#entrada_radio_sim').removeClass("show");
            $('#entrada_radio_sim').addClass("hide");

            $('#cliente').val("");

          }else{
            $('#entrada_radio_nao').removeClass("show");
            $('#entrada_radio_nao').addClass("hide");

            $('#entrada_radio_sim').removeClass("hide");
            $('#entrada_radio_sim').addClass("show");

            $('#fornecedor').val("");
          }
        }
        
		</script>

