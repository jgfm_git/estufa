<?php
require_once('conn/conexao.php'); 
session_start();



$n = 0;



if(!empty($_SESSION['ZWxldHJpY2Ft'])){
  $usuario_id = $_SESSION['ZWxldHJpY2Ft'];
}else{
	header('Location: login.php');
}



$sql = "select * from user where id = $usuario_id";

$res = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($res)){
  $id = $row['id'];
  $nome   = $row['nome'];
  $email  = $row['email'];
  $image  = $row['avatar'];
  if(strlen($image) > 2){
    $image  = $row['avatar'];
  }else{
    $image = "img/avatar.png";
  }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="theme-color" content="#000">
  <title>Gestão | Estufa</title>

  <!-- Custom fonts for this template-->
  <link href="css/toast.css" rel="stylesheet" type="text/css">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  <meta name="mobile-web-app-capable" content="yes">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link href="img/logo_seco.png" rel="shortcut icon">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">
	
	<style>
  body {
    font-size: 0.85rem;
  }
  .status{
    padding: 3px;
    color: #fff; 
  }
  table.dataTable tbody th, table.dataTable tbody td {
    vertical-align: middle;
  }
	.zoom:hover {
		transform: scale(1.03);
	}
	
	.card-hover:hover {
		background: #eeefff;
	}
	.ui-autocomplete-input {
	  z-index: 1511;
	  position: relative;
	}
	.ui-menu .ui-menu-item a {
	  font-size: 12px;
	}
	.ui-autocomplete {
	  position: absolute;
	  top: 0;
	  left: 0;
	  z-index: 1510 !important;
	  float: left;
	  display: none;
	  list-style: none;
	  -webkit-border-radius: 2px;
	  -moz-border-radius: 2px;
	  border-radius: 2px;
	  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	  -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	  -webkit-background-clip: padding-box;
	  -moz-background-clip: padding;
	  background-clip: padding-box;
	  *border-right-width: 2px;
	  *border-bottom-width: 2px;
	}
	.ui-menu-item > a.ui-corner-all {
		display: block;
		padding: 3px 15px;
		clear: both;
		font-weight: normal;
		line-height: 18px;
		color: #555555;
		white-space: nowrap;
		text-decoration: none;
	}
	.ui-state-hover, .ui-state-active {
		  color: #ffffff;
		  text-decoration: none;
		  background-color: #0088cc;
		  border-radius: 0px;
		  -webkit-border-radius: 0px;
		  -moz-border-radius: 0px;
		  background-image: none;
	}
  table.dataTable {
    border-collapse: collapse !important;
  }
  .collapse-item{
    cursor: pointer;
  }
  .nav-item{
    cursor: pointer;
  }

  table.dataTable tbody tr{
    background-image: linear-gradient(180deg,#5a5c69 10%,#505155 100%);
  }
  label {
    color: white;
  }

  .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active{
        color: white !important;
  }

  .dataTables_wrapper .dataTables_paginate .paginate_button {
      color: white !important;
  }

  .spinner-border{
    width: 3rem;
    height: 3rem;
  }
  .show{
		display: block;
	}
	.hide{
		display: none;
	}
	</style>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper" >

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php" >
        <div class="sidebar-brand-icon">
          <img id="logo-seco" src="img/logo_lateral.png" style="width: 120px;height: 0px;display:none" />
        </div>
        <div class="sidebar-brand-text mx-3"><img src="img/logo_lateral.png" style="padding: 13px;width: 230px;height: 100px;" /></div>
      </a>

      <!-- Divider -->

      <!-- Divider 
      <hr class="sidebar-divider">-->

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item active">
        <a class="nav-link collapsed" href="#dashboard" onclick="page('dashboard')" >
          <i class="fa fa-tachometer-alt text-white" ></i>
          <span>Painel de Controle</span>
        </a>
      </li> 
      <li class="nav-item active">
        <a class="nav-link collapsed" href="#orcamento" onclick="page('orcamento')" >
          <i class="fa fa-clipboard-list text-white" ></i>
          <span>Orçamentos</span>
        </a>
       </li>
	    <hr class="sidebar-divider">
	    <div class="sidebar-heading">
        Gestão
      </div>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEntrada" aria-expanded="true" aria-controls="collapseEntrada">
          <i class="fas fa-dolly text-white"></i>
          <span>&nbspEntradas</span>
        </a>
        <div id="collapseEntrada" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Entradas:</h6>
            <a class="collapse-item" data-toggle="modal" data-target="#AddEstoque">Cadastro de Entrada</a>
            <a class="collapse-item" href="#entrada" onclick="page('entrada')">Listagem de Entradas</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEstoque" aria-expanded="true" aria-controls="collapseEstoque">
          <i class="fas fa-clipboard-list text-white"></i>
          <span>&nbspEstoque</span>
        </a>
        <div id="collapseEstoque" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Estoque:</h6>
            <a class="collapse-item" href="#estoque_cliente" onclick="page('estoque_cliente')">Estoque de Clientes</a>
            <a class="collapse-item" href="#estoque_estufa" onclick="page('estoque_estufa')">Estoque da Estufa</a>
          </div>
        </div>
      </li>
  
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSemeacao" aria-expanded="true" aria-controls="collapseSemeacao">
          <i class="fas fa-seedling text-white"></i>
          <span>&nbspSemeação</span>
        </a>
        <div id="collapseSemeacao" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Semeação:</h6>
            <a class="collapse-item" href="#semeacao" onclick="page('semeacao')">Listagem de Semeação</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProdutos" aria-expanded="true" aria-controls="collapseProdutos">
          <i class="fas fa-leaf text-white"></i>
          <span>&nbspSementes</span>
        </a>
        <div id="collapseProdutos" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Sementes:</h6>
            <a class="collapse-item" data-toggle="modal" data-target="#AddProd">Cadastro de Semente</a>
            <a class="collapse-item" href="#produto" onclick="page('produto')">Listagem de Sementes</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCliente" aria-expanded="true" aria-controls="collapseCliente">
          <i class="fa fa-users text-white"></i>
          <span>&nbspClientes</span>
        </a>
        <div id="collapseCliente" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Clientes:</h6>
            <a class="collapse-item" onclick="geraClienteIndex()" >Cadastro Cliente</a>
            <a class="collapse-item" href="#cliente" onclick="page('cliente')" >Listagem de Clientes</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFornecedor" aria-expanded="true" aria-controls="collapseFornecedor">
          <i class="fa fa-boxes text-white"></i>
          <span>&nbsp&nbspFornecedores</span>
        </a>
        <div id="collapseFornecedor" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Fornecedores:</h6>
            <a class="collapse-item" data-toggle="modal" data-target="#AddFor" >Cadastro Fornecedor</a>
            <a class="collapse-item" href="#fornecedor" onclick="page('fornecedor')">Listagem de Fornecedores</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFunc" aria-expanded="true" aria-controls="collapseFunc">
          <i class="fas fa-address-card text-white"></i>
          <span>&nbspFuncionários</span>
        </a>
        <div id="collapseFunc" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Funcionários:</h6>
            <a class="collapse-item" data-toggle="modal" data-target="#AddFunc" >Cadastro Funcionário</a>
            <a class="collapse-item" href="#funcionario" onclick="page('funcionario')">Listagem de Funcionários</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBarraca" aria-expanded="true" aria-controls="collapseBarraca">
          <i class="fa fa-box text-white"></i>
          <span>&nbspBarracas</span>
        </a>
        <div id="collapseBarraca" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Barracas:</h6>
            <a class="collapse-item" data-toggle="modal" data-target="#AddBarraca">Cadastro Barraca</a>
            <a class="collapse-item" href="#barraca" onclick="page('barraca')" >Listagem de Barracas</a>
          </div>
        </div>
      </li>
 
      <hr class="sidebar-divider">
	    <div class="sidebar-heading">
        Financeiro
      </div>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#contas_receber" onclick="page('contas_receber')" >
          <i class="fa fa-barcode text-white" ></i>
          <span>&nbspContas a Receber</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#contas_pagar" onclick="page('contas_pagar')" >
          <i class="fa fa-barcode text-white" ></i>
          <span>&nbspContas a Pagar</span>
        </a>
      </li>
  
      <!-- Divider -->
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Relatórios
      </div>

      <li class="nav-item">
        <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseRelOS" aria-expanded="true" aria-controls="collapseRelOS">
          <i class="fa fa-print text-white"></i>
          <span>&nbspRelatórios</span>
        </a>
        <div id="collapseRelOS" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header" >Relatórios</h6>
            <a class="collapse-item" onclick="page('#')" >Produção</a>
            <a class="collapse-item" onclick="page('#')" >Entrada e Saída</a>
          </div>
        </div>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">

	    <div class="sidebar-heading">
        Dados
      </div>
 
	   
      <li class="nav-item">
        <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseConta" aria-expanded="true" aria-controls="collapseConta">
          <i class="fa fa-user text-white"></i>
          <span>&nbspMinha Conta</span>
        </a>
        <div id="collapseConta" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header" >Minha conta:</h6>
            <a class="collapse-item" onclick="page('minha_conta')" >Dados</a>
          </div>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseMinhaEmpresa" aria-expanded="true" aria-controls="collapseMinhaEmpresa">
          <i class="fa fa-building text-white"></i>
          <span>&nbspMinha Empresa</span>
        </a>
        <div id="collapseMinhaEmpresa" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header" >Minha empresa:</h6>
            <a class="collapse-item" onclick="page('minha_empresa')" >Dados</a>
          </div>
        </div>
      </li>


    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link  rounded-circle mr-3 " onclick="logo()">
            <i class="fa fa-bars text-white"></i>
          </button>

     
	    	<!-- Topbar Navbar -->

        <!-- ANOTAÇÃO E SYNC -->
          
          <div style="width: 100%;text-align-last: end;"><button data-toggle="modal" data-target="#AddAnotacao" class="btn btn-primary">Anotar</button></div>
          
        
        <!-- FIM ANOTAÇÃO E SYNC -->

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

          
            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
         
            
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-white "><?php echo utf8_encode($nome);?></span>
                <img class="rounded-circle" style="width: 60px" src="<?php echo $image;?>" />
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
              <!--  <a class="dropdown-item" href="#">
                  <i class="fas fa-key fa-sm fa-fw mr-2 text-gray-400"></i>
                  Alterar Senha
                </a> -->
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
          </ul>
        </nav>
        <!-- End of Topbar -->
 
        
			<div id="conteudo">
      
   
		   
      </div>
	

		 
    <!-- End of Content Wrapper -->
     <!-- Footer -->
      <footer class="sticky-footer" >
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; EvolutionSoft <?php echo date('Y');?></span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->
  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Logout</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Você realmente deseja sair ?</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Não</button>
          <a class="btn btn-primary" href="php/logout.php">Sim</a>
        </div>
      </div>
    </div>
  </div>
  

 <!-- Logout Modal-->
 <div class="modal fade" id="SyncModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="align-self: center;">
          <h5 class="modal-title" id="exampleModalLabel">Atualização de Sitema</h5>
        </div>
        <div class="modal-body" style="margin-top: 20px;margin-bottom: 20px;text-align: center;">
        
        <div id="conteudo-sync">Aguarde enquanto o sistema atualiza ...</div>
        
        </div>
       
      </div>
    </div>
  </div>
  
<?php
if(isset($_SESSION['msg'])){ 
 $n = 1;
?>
<div id="snackbar"  >
<?php
	echo $_SESSION['msg'];
	unset($_SESSION['msg']);
?>
</div>
<?php }  ?>




  
  <!-- Bootstrap core JavaScript-->
  
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts 
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>  -->
  <!--<script src="//code.jquery.com/jquery-1.12.4.js"></script>-->
  <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/js/standalone/selectize.min.js"></script>
  </body>

  <!-- MODAL DE CADASTRO  -->

  <?php
     include_once('views/modals/cad_cliente.php'); 
     include_once('views/modals/cad_fornecedor.php'); 
     include_once('views/modals/cad_produto.php'); 
     include_once('views/modals/cad_anotacao.php');
     include_once('views/modals/cad_orcamento.php');
     include_once('views/modals/cad_barraca.php');
     include_once('views/modals/cad_funcionario.php');
     include_once('views/modals/cad_entrada.php'); 
  ?>

  <!-- FIM DO MODAL DE CADASTRO  -->
<script>
var l = 1;
var url = window.location.href;
var id = url.substring(url.lastIndexOf('#') + 1);
var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
		$("#conteudo").html(data);
if(id.length > 0){
  
  if(id.length == 43){
    $(document).ready(function(){
		  $('#conteudo').load("views/dashboard.php");			
	  });
  }else{
    $(document).ready(function(){
	  	$('#conteudo').load("views/"+id+".php");			
  	});
  }
	
}else{
	$(document).ready(function(){
		$('#conteudo').load("views/dashboard.php");			
	});
}


var t = <?php echo $n;?>;
if(t > 0){
	myFunction();
}	
function myFunction() {
  // Get the snackbar DIV
  var x = document.getElementById("snackbar");

  // Add the "show" class to DIV
  x.className = "show";

  // After 3 seconds, remove the show class from DIV
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}
function page(pagina){
  var data = "<div id='spinner' class='spinner-border' role='status' style='margin-left: 47%;margin-top: 20%;margin-bottom: 20%'><span class='sr-only'>Loading...</span></div>";
		$("#conteudo").html(data);
	$(document).ready(function(){
		$('#conteudo').load("views/"+pagina+".php");			
	});
}

function geraClienteIndex(){
  $("#id_orcamento_cli").val(0);
  $("#razao").val("");
  $("#responsavel").val("");
  $("#AddCli").modal('show');
}

function logo(){
  l++;

  if(l % 2 == 0){
      $('#logo-seco').css('display','block');
  }else{
      $('#logo-seco').css('display','none');
  }
}
</script>


</html>
